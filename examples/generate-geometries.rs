use std::fs::File;

use clap::{command, Parser};
use dyntri::{
    graph::{spacetime_graph::SpacetimeGraph, GenericGraph},
    monte_carlo::fixed_universe::Universe,
    triangulation::FullTriangulation,
};
use log::info;
use rand::SeedableRng;
use rand_xoshiro::Xoshiro256PlusPlus;

#[derive(Parser)]
#[command(author, version, about, long_about)]
struct Cli {
    /// Sets the base path for the output geometries
    #[arg(short = 'p', long, default_value = "cdt", value_name = "BASEPATH")]
    basepath: String,
    #[arg(short = 'n', long)]
    ntriangles: usize,
    #[arg(short = 't', long)]
    ntime: usize,
    #[arg(short = 'N', long)]
    ngeometries: usize,
    #[arg(short = 's', long)]
    seed: Option<u64>,
    /// Burnin time in terms of sweeps
    #[arg(long, default_value = "5000.0")]
    burnin: f64,
    /// Number of sweeps inbetween geometry outputs
    #[arg(long, default_value = "200.0")]
    thinning: f64,
}

fn main() -> anyhow::Result<()> {
    let mut log_builder = env_logger::Builder::from_default_env();
    log_builder.filter_level(log::LevelFilter::Debug).init();

    let args = Cli::parse();

    // Create CDT configuration
    info!("Creating CDT configuration");
    let mut rng = if let Some(seed) = args.seed {
        Xoshiro256PlusPlus::seed_from_u64(seed)
    } else {
        Xoshiro256PlusPlus::from_entropy()
    };
    let mut universe = Universe::default(args.ntriangles, args.ntime);

    let sweep = args.ntriangles as f64;

    info!("Starting burn-in");
    for _ in 0..((args.burnin * sweep) as usize) {
        universe.step(&mut rng);
    }
    info!("Finished burn-in");
    for i in 0..args.ngeometries {
        for _ in 0..((args.thinning * sweep) as usize) {
            universe.step(&mut rng);
        }

        let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
        let graph = GenericGraph::from_spacetime_graph(&SpacetimeGraph::from_triangulation_vertex(
            &triangulation,
        ));
        let filename = format!(
            "{}-N{}-T{}-{:0>4}.adj",
            args.basepath, args.ntriangles, args.ntime, i
        );
        info!("Save geometry to {}", filename);
        let file = File::create(filename)?;
        dyntri::io::graph::export_adjacency_list(file, &graph)?;
    }
    info!("Finished simulation");

    Ok(())
}
