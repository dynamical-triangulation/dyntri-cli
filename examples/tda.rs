use std::{fs::File, io::BufWriter};

use dyntri::{
    graph::{
        periodic_spacetime_graph::PeriodicSpacetimeGraph, spacetime_graph::SpacetimeGraph,
        GenericGraph,
    },
    io,
    monte_carlo::fixed_universe::Universe,
    triangulation::FullTriangulation,
    visualisation::periodic_embedding2d::PeriodicEmbedding2D,
};
use log::info;
use rand::SeedableRng;
use rand_xoshiro::Xoshiro256PlusPlus;

// const SLICE_LEN: usize = 50;
// const TIME_LEN: usize = 50;
const SLICE_LEN: usize = 30;
const TIME_LEN: usize = 30;

const TRIANGLE_LEN: usize = SLICE_LEN * TIME_LEN * 2;
const MC_STEPS: usize = TRIANGLE_LEN * 200;

fn main() -> anyhow::Result<()> {
    let mut log_builder = env_logger::Builder::from_default_env();
    log_builder.filter_level(log::LevelFilter::Debug).init();

    // Create CDT configuration
    info!("Creating CDT configuration");
    let mut rng = Xoshiro256PlusPlus::seed_from_u64(1);
    let mut universe = Universe::default(TRIANGLE_LEN, TIME_LEN);

    for _ in 0..MC_STEPS {
        universe.step(&mut rng);
    }

    // Extract triangulation and vertex graph
    info!("Create graph");
    let triangulation = FullTriangulation::from_triangulation(&universe.triangulation);
    let spacetime_graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);
    let graph = GenericGraph::from_spacetime_graph(&SpacetimeGraph::from_triangulation_vertex(
        &triangulation,
    ));

    // Export adjacency
    info!("Export adjacency");
    let size_label = human_format::Formatter::new()
        .with_separator("")
        .with_decimals(0)
        .format(TRIANGLE_LEN as f64);
    let adj_file = File::create(format!("cdt-N{}.adj", size_label))?;
    io::graph::export_adjacency_list(BufWriter::new(adj_file), &graph)?;

    // Create embedding
    info!("Create embedding");
    let mut embedding = PeriodicEmbedding2D::initialize(&triangulation, &spacetime_graph);
    embedding.shift_slices();
    embedding.make_harmonic();

    // Export embedding
    info!("Export embedding");
    let embedding_file = File::create(format!("cdt-N{}.viz", size_label))?;
    let embedding = embedding.export(9);
    embedding.write(&mut BufWriter::new(embedding_file))?;

    Ok(())
}
