use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};
use dyntri::observables::volume_profile;
use dyntri::triangulation::FullTriangulation;
use log::{info, warn};
use ndarray::prelude::*;

const VOLUME_PROFILE_DATASET_NAME: &str = "volume-profile";

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;

    // Volume profile
    file.new_dataset::<f32>()
        .shape([parameters.measurements, parameters.timeslices])
        .create(VOLUME_PROFILE_DATASET_NAME)?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    let vprofile = arr1(volume_profile::measure(&full_triangulation).volumes());

    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let vprofile_dataset = datafile.dataset(VOLUME_PROFILE_DATASET_NAME)?;
    vprofile_dataset.write_slice(vprofile.view(), s![i, ..])?;
    datafile.close()?;

    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;
    info!("Finished observable measurements");

    Ok(())
}
