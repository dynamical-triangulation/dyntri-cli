use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

use anyhow::Context;
use dyntri::graph::SampleGraph;
use dyntri::observables::average_sphere_distance::double_sphere;
use dyntri::triangulation::FullTriangulation;
use dyntri::{
    graph::periodic_spacetime_graph::PeriodicSpacetimeGraph,
    observables::average_sphere_distance::single_sphere,
};
use log::{debug, info, warn};
use ndarray::prelude::*;

const ASD_DOUBLE_DATASET_NAME: &str = "average-sphere-distance-double";
const ASD_SINGLE_DATASET_NAME: &str = "average-sphere-distance-single";
const VIOLATION_DOUBLE_DATASET_NAME: &str = "asd-violations-double";
const VIOLATION_SINGLE_DATASET_NAME: &str = "asd-violations-single";

const SAMPLE_SIZE_DOUBLE: usize = 10;
const SAMPLE_SIZE_SINGLE: usize = 10;

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    let max_distance = obs_parameters.max_distance as f64;
    let max_double_delta = (max_distance / 3.0) as usize;
    let max_single_delta = (max_distance / 2.7) as usize;

    debug!("Preparing average sphere distance sampled dataset");
    // Average sphere distance double
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            SAMPLE_SIZE_DOUBLE,
            max_double_delta,
        ])
        .chunk([1, SAMPLE_SIZE_DOUBLE, max_double_delta])
        .blosc_lz4(9, true)
        .create(ASD_DOUBLE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            SAMPLE_SIZE_DOUBLE,
            max_double_delta,
        ])
        .chunk([1, SAMPLE_SIZE_DOUBLE, max_double_delta])
        .blosc_lz4(9, true)
        .create(VIOLATION_DOUBLE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    // Average sphere distance single
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            SAMPLE_SIZE_DOUBLE,
            max_single_delta,
        ])
        .chunk([1, SAMPLE_SIZE_DOUBLE, max_single_delta])
        .blosc_lz4(9, true)
        .create(ASD_SINGLE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            SAMPLE_SIZE_DOUBLE,
            max_single_delta,
        ])
        .chunk([1, SAMPLE_SIZE_DOUBLE, max_single_delta])
        .blosc_lz4(9, true)
        .create(VIOLATION_SINGLE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> anyhow::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let periodic_graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&full_triangulation);

    // Average sphere distance double
    let max_distance = sim.observable_parameters.max_distance as f64;
    let max_double_delta = (max_distance / 3.0) as usize;
    let max_single_delta = (max_distance / 2.7) as usize;

    info!(
        "Measuring {SAMPLE_SIZE_DOUBLE} samples of the single average sphere profile\
        on the graph, up to delta {max_double_delta}"
    );
    let deltas: Vec<usize> = (1..max_double_delta + 1).collect();

    info!("Measuring ASD double");
    let mut asd_profile = Array2::zeros([0, deltas.len()]);
    let mut asd_violations = Array2::zeros([0, deltas.len()]);
    for _ in 0..SAMPLE_SIZE_DOUBLE {
        let point = periodic_graph.sample_node(&mut sim.rng);
        let asd =
            double_sphere::profile(&periodic_graph, point, deltas.iter().copied(), &mut sim.rng);
        asd_profile
            .push_row(arr1(&asd.profile).view())
            .context("Adding ASD delta onto profile")?;
        asd_violations
            .push_row(arr1(&asd.violation_ratios).view())
            .context("Adding ASD delta onto profile")?;
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_DOUBLE_DATASET_NAME)?;
    dataset.write_slice(&asd_profile, s![i, .., ..])?;
    let dataset = datafile.dataset(VIOLATION_DOUBLE_DATASET_NAME)?;
    dataset.write_slice(&asd_violations, s![i, .., ..])?;
    datafile.close()?;

    // Average sphere distance single
    info!(
        "Measuring {SAMPLE_SIZE_SINGLE} samples of the single average sphere profile\
        on the graph, up to delta {max_single_delta}."
    );
    let deltas: Vec<usize> = (1..max_single_delta + 1).collect();

    info!("Measuring ASD single");
    let mut asd_profile = Array2::zeros([0, deltas.len()]);
    let mut asd_violations = Array2::zeros([0, deltas.len()]);
    for _ in 0..SAMPLE_SIZE_SINGLE {
        let point = periodic_graph.sample_node(&mut sim.rng);
        let asd = single_sphere::profile(&periodic_graph, point, deltas.iter().copied());
        asd_profile
            .push_row(arr1(&asd.profile).view())
            .context("Adding ASD delta onto profile")?;
        asd_violations
            .push_row(arr1(&asd.violation_ratios).view())
            .context("Adding ASD delta onto profile")?;
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_SINGLE_DATASET_NAME)?;
    dataset.write_slice(&asd_profile, s![i, .., ..])?;
    let dataset = datafile.dataset(VIOLATION_SINGLE_DATASET_NAME)?;
    dataset.write_slice(&asd_violations, s![i, .., ..])?;
    datafile.close()?;

    info!("Finished observable measurements");
    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    Ok(())
}
