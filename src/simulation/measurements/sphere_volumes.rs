use dyntri::{
    graph::{
        periodic_spacetime_graph::{PeriodicSpacetimeGraph, PeriodicSpacetimeLabel},
        spacetime_graph::SpacetimeGraph,
        SampleGraph,
    },
    observables::distance_profile,
    triangulation::FullTriangulation,
};
use log::{info, warn};
use ndarray::prelude::*;

use crate::simulation::Simulation;

const MANIFOLD_SAMPLES: usize = 50;
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const SPHERE_VOLUMES_PERIODIC_DATASET_NAME: &str = "sphere-volumes-periodic";
const SPHERE_VOLUMES_OVERLAP_DATASET_NAME: &str = "sphere-volumes-overlap";

pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;
    let max_distance = simulation.observable_parameters.max_distance;

    // Sphere volumes
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.measurements, MANIFOLD_SAMPLES, max_distance + 1])
        .chunk([1, MANIFOLD_SAMPLES, max_distance + 1])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..max_distance + 1))
        .create("r")?;

    // Sphere volumes periodic
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.measurements, MANIFOLD_SAMPLES, max_distance + 1])
        .chunk([1, MANIFOLD_SAMPLES, max_distance + 1])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_PERIODIC_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..max_distance + 1))
        .create("r")?;

    // Sphere volumes periodic overlap
    let dataset = file
        .new_dataset::<f64>()
        .shape([parameters.measurements, MANIFOLD_SAMPLES, max_distance + 1])
        .chunk([1, MANIFOLD_SAMPLES, max_distance + 1])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_OVERLAP_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..max_distance + 1))
        .create("r")?;

    Ok(())
}

pub fn perform_measurements(simulation: &mut Simulation, i: usize) -> hdf5::Result<()> {
    let max_distance = simulation.observable_parameters.max_distance;

    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&simulation.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let graph = SpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let periodic_graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&full_triangulation);

    let points = Array1::from_iter(
        std::iter::repeat_with(|| graph.sample_node(&mut simulation.rng)).take(MANIFOLD_SAMPLES),
    );
    info!("Measuring sphere volumes up to r={max_distance}");
    let mut sphere_volumes = Array2::zeros([0, max_distance + 1]);
    for &point in points.iter() {
        let sphere_vol = Array1::from_vec(
            distance_profile::measure(&graph, point, max_distance)
                .profile
                .into_vec(),
        );
        sphere_volumes
            .push_row(sphere_vol.view())
            .expect("Shape should be right by construction");
    }

    warn!("Writing to datafile do NOT terminate the program");
    let file = simulation.open_datafile()?;
    let svol_dataset = file.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    svol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    file.close()?;

    info!("Measuring periodic sphere volumes up to r={max_distance}");
    let mut sphere_volumes = Array2::zeros([0, max_distance + 1]);
    let mut sphere_overlaps = Array2::zeros([0, max_distance + 1]);
    for &point in points.iter() {
        let point = PeriodicSpacetimeLabel::at_origin(point);
        let svol_profile = distance_profile::measure_periodic(&periodic_graph, point, max_distance);
        sphere_volumes
            .push_row(svol_profile.profile.view())
            .expect("Shape should be right by construction");
        sphere_overlaps
            .push_row(svol_profile.overlap.view())
            .expect("Shape should be right by construction");
    }

    warn!("Writing to datafile do NOT terminate the program");
    let file = simulation.open_datafile()?;
    let svol_dataset = file.dataset(SPHERE_VOLUMES_PERIODIC_DATASET_NAME)?;
    svol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    let soverlap_dataset = file.dataset(SPHERE_VOLUMES_OVERLAP_DATASET_NAME)?;
    soverlap_dataset.write_slice(&sphere_overlaps, s![i, .., ..])?;
    file.close()?;

    info!("Finished measurements");

    Ok(())
}
