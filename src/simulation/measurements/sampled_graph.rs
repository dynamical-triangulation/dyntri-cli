use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};
use dyntri::graph::periodic_spacetime_graph::bounded_graph::BoundedPeriodicSpacetimeGraph;
use dyntri::graph::periodic_spacetime_graph::PeriodicSpacetimeGraph;
use dyntri::observables::diffusion;
use dyntri::{
    graph::{Graph, SampleGraph},
    triangulation::FullTriangulation,
};
use log::{debug, info, warn};
use ndarray::prelude::*;

// const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
// const SPHERE_VOLUMES_PERIODIC_DATASET_NAME: &str = "sphere-volumes-periodic";
// const RETURN_PROBABILITY_DATASET_NAME: &str = "return-probability";
const RETURN_PROBABILITY_PERIODIC_DATASET_NAME: &str = "return-probability-periodic";
const DIFFUSION_COEFFICIENT: f64 = 0.90;
// const SPHERE_VOLUME_SAMPLES: usize = 500;
const DIFFUSION_SAMPLES: usize = 1;
const MAX_T_PATCHES: u8 = 1;
const MAX_X_PATCHES: u8 = 2;

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    // // Sphere volumes
    // let dataset = file
    //     .new_dataset::<usize>()
    //     .shape([
    //         parameters.measurements,
    //         SPHERE_VOLUME_SAMPLES,
    //         obs_parameters.max_distance + 1,
    //     ])
    //     .chunk([1, SPHERE_VOLUME_SAMPLES, obs_parameters.max_distance + 1])
    //     .blosc_lz4(9, true)
    //     .create(SPHERE_VOLUMES_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
    //     .create("r")?;

    // // Sphere volumes on periodic graph
    // let dataset = file
    //     .new_dataset::<usize>()
    //     .shape([
    //         parameters.measurements,
    //         SPHERE_VOLUME_SAMPLES,
    //         obs_parameters.max_distance + 1,
    //     ])
    //     .chunk([1, SPHERE_VOLUME_SAMPLES, obs_parameters.max_distance + 1])
    //     .blosc_lz4(9, true)
    //     .create(SPHERE_VOLUMES_PERIODIC_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
    //     .create("r")?;

    // // Return probabilty
    // let dataset = file
    //     .new_dataset::<f32>()
    //     .shape([
    //         parameters.measurements,
    //         DIFFUSION_SAMPLES,
    //         obs_parameters.max_diffusion_time + 1,
    //     ])
    //     .chunk([1, DIFFUSION_SAMPLES, obs_parameters.max_diffusion_time + 1])
    //     .blosc_lz4(9, true)
    //     .create(RETURN_PROBABILITY_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&arr0(0))
    //     .create("sigma-start")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&arr0(obs_parameters.max_diffusion_time))
    //     .create("sigma-max")?;

    // Return probabilty
    let dataset = file
        .new_dataset::<f32>()
        .shape([
            parameters.measurements,
            DIFFUSION_SAMPLES,
            obs_parameters.max_diffusion_time + 1,
        ])
        .chunk([1, DIFFUSION_SAMPLES, obs_parameters.max_diffusion_time + 1])
        .blosc_lz4(9, true)
        .create(RETURN_PROBABILITY_PERIODIC_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(0))
        .create("sigma-start")?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(obs_parameters.max_diffusion_time))
        .create("sigma-max")?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    // let graph = SpacetimeGraph::from_triangulation_dual(&full_triangulation);
    let periodic_graph = PeriodicSpacetimeGraph::from_triangulation_dual(&full_triangulation);
    let bounded_periodic_graph = BoundedPeriodicSpacetimeGraph::from_periodic_graph(
        periodic_graph,
        MAX_T_PATCHES,
        MAX_X_PATCHES,
    );

    // // Sample a set of points in the graph to use for sphere_volume measurements
    // let points: Vec<<PeriodicSpacetimeGraph as Graph>::Label> = (0..SPHERE_VOLUME_SAMPLES)
    //     .map(|_| periodic_graph.sample_node(&mut sim.rng))
    //     .collect();

    // let max_distance = sim.observable_parameters.max_distance;
    // let mut sphere_volumes = Array2::zeros([0, max_distance + 1]);
    // debug!("Measuring sphere volumes in normal graph");
    // for &point in points.iter() {
    //     let dprofile = distance_profile::measure(&graph, point, max_distance).profile;
    //     let dprofile = Array1::from_vec(dprofile.into_vec());
    //     sphere_volumes
    //         .push_row(dprofile.view())
    //         .expect("Shape should be correct by construction");
    // }
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    // sphere_vol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    // datafile.close()?;

    // let mut sphere_volumes_periodic = Array2::zeros([0, max_distance + 1]);
    // debug!("Measuring sphere volumes in periodic cut-open graph");
    // for &point in points.iter() {
    //     let dprofile = distance_profile::measure(&periodic_graph, point, max_distance).profile;
    //     let dprofile = Array1::from_vec(dprofile.into_vec());
    //     sphere_volumes_periodic
    //         .push_row(dprofile.view())
    //         .expect("Shape should be correct by construction");
    // }
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let sphere_vol_periodic_dataset = datafile.dataset(SPHERE_VOLUMES_PERIODIC_DATASET_NAME)?;
    // sphere_vol_periodic_dataset.write_slice(&sphere_volumes_periodic, s![i, .., ..])?;
    // datafile.close()?;

    // // Sample a set of points in the graph to use for diffusion measurements
    let points: Vec<<BoundedPeriodicSpacetimeGraph as Graph>::Label> = (0..DIFFUSION_SAMPLES)
        .map(|_| bounded_periodic_graph.sample_node(&mut sim.rng))
        .collect();

    let max_diffusion_time = sim.observable_parameters.max_diffusion_time;
    // let mut return_probability = Array2::zeros([0, max_diffusion_time + 1]);
    // debug!("Measuring return probability on graph");
    // for &point in points.iter() {
    //     let rprop = Array1::from_vec(
    //         diffusion::measure_sized(&graph, point, max_diffusion_time + 1, DIFFUSION_COEFFICIENT)
    //             .0,
    //     );
    //     return_probability
    //         .push_row(rprop.view())
    //         .expect("Shapes should be correct by construction")
    // }
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let return_prob_dataset = datafile.dataset(RETURN_PROBABILITY_DATASET_NAME)?;
    // return_prob_dataset.write_slice(&return_probability, s![i, .., ..])?;
    // datafile.close()?;

    debug!("Measuring return probability on periodic graph");
    for (j, &point) in points.iter().enumerate() {
        let rprop = Array1::from_vec(
            diffusion::measure_bounded(
                &bounded_periodic_graph,
                point,
                max_diffusion_time + 1,
                DIFFUSION_COEFFICIENT,
            )
            .0,
        );

        warn!("Writing observables to disk. Do NOT stop the simulation");
        let datafile = sim.open_datafile()?;
        let return_prob_dataset = datafile.dataset(RETURN_PROBABILITY_PERIODIC_DATASET_NAME)?;
        return_prob_dataset.write_slice(rprop.view(), s![i, j, ..])?;
        datafile.close()?;
    }

    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;
    info!("Finished observable measurements");

    Ok(())
}
