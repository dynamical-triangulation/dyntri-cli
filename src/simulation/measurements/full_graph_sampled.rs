use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

use dyntri::collections::Label;
use dyntri::graph::SampleGraph;
use dyntri::graph::{spacetime_graph::SpacetimeGraph, GenericGraph};
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::{average_sphere_distance, vertex_degree_distribution};
use dyntri::triangulation::FullTriangulation;
use log::{debug, info, warn};
use ndarray::prelude::*;

const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
const ASD_DATASET_NAME: &str = "average-sphere-distance";
const ASD_SIMPLE_DATASET_NAME: &str = "average-sphere-distance-simple";
// const ASD_FULL_DATASET_NAME: &str = "average-sphere-distance-full";
const DEGREE_DATASET_NAME: &str = "vertex-degree-field";
const NBR_DEGREE_DATASET_NAME: &str = "average-neighbour-degree-field";
const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
// const SAMPLES2P_UNIFORM_DATASET_NAME: &str = "two-point-samples-uniform";
// const SAMPLES2P_ORIGIN_DATASET_NAME: &str = "two-point-samples-origin";
const SAMPLES_DATASET_NAME: &str = "point-samples";

// const CORRELATION_DELTAS: std::ops::Range<usize> = 10..21;
const SAMPLE_SIZE: usize = 5000;
// const TWO_POINT_SAMPLE_SIZE: usize = 10_000;

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let vertex_count = simulation.parameters.size / 2;
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    debug!("Preparing average sphere distance sampled dataset");
    // Average sphere distance sampled
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    debug!("Preparing average sphere distance using the old way dataset");
    // Average sphere distance sampled
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_SIMPLE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    // debug!("Preparing average sphere distance full graph dataset");
    // let cor_delta_amount = CORRELATION_DELTAS.len();
    // // Average sphere distance full graph
    // let dataset = file
    //     .new_dataset::<f64>()
    //     .shape([parameters.measurements, cor_delta_amount, vertex_count])
    //     .chunk([1, cor_delta_amount, vertex_count])
    //     .blosc_lz4(9, true)
    //     .create(ASD_FULL_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(CORRELATION_DELTAS))
    //     .create("delta")?;

    debug!("Preparing vertex degree field dataset");
    // Vertex degree field
    file.new_dataset::<u16>()
        .shape([parameters.measurements, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(DEGREE_DATASET_NAME)?;

    debug!("Preparing average neighbour vertex degree field dataset");
    // Average neighbour vertex degree field
    file.new_dataset::<f32>()
        .shape([parameters.measurements, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(NBR_DEGREE_DATASET_NAME)?;

    debug!("Preparing pair distance distribution dataset");
    // Pair distance distribution
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.measurements, obs_parameters.max_distance + 1])
        .chunk([1, obs_parameters.max_distance + 1])
        .blosc_lz4(9, true)
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    debug!("Preparing sphere volume dataset");
    // Sphere volumes
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            parameters.measurements,
            obs_parameters.max_distance + 1,
            vertex_count,
        ])
        .chunk([1, obs_parameters.max_distance + 1, vertex_count])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    debug!("Preparing point sample dataset");
    // Point samples
    file.new_dataset::<usize>()
        .shape([parameters.measurements, SAMPLE_SIZE])
        .chunk([1, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(SAMPLES_DATASET_NAME)?;

    // debug!("Preparing two-point sample uniform dataset");
    // // Correlation/two-point samples uniform
    // file.new_dataset::<usize>()
    //     .shape([
    //         2,
    //         parameters.measurements,
    //         obs_parameters.max_correlation_distance,
    //         TWO_POINT_SAMPLE_SIZE,
    //     ])
    //     .chunk([
    //         1,
    //         1,
    //         obs_parameters.max_correlation_distance,
    //         TWO_POINT_SAMPLE_SIZE,
    //     ])
    //     .blosc_lz4(9, true)
    //     .create(SAMPLES2P_UNIFORM_DATASET_NAME)?;

    // debug!("Preparing two-point sample origin based dataset");
    // // Correlation/two-point samples from origin
    // file.new_dataset::<(usize, usize)>()
    //     .shape([
    //         2,
    //         parameters.measurements,
    //         obs_parameters.max_correlation_distance,
    //         TWO_POINT_SAMPLE_SIZE,
    //     ])
    //     .chunk([
    //         1,
    //         1,
    //         obs_parameters.max_correlation_distance,
    //         TWO_POINT_SAMPLE_SIZE,
    //     ])
    //     .blosc_lz4(9, true)
    //     .create(SAMPLES2P_ORIGIN_DATASET_NAME)?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let graph = GenericGraph::from_spacetime_graph(&spacetime_graph);

    let mut temp_filepath = sim.observable_parameters.temp_dir.clone();
    debug!("Temporary directory used is {:?}", &temp_filepath);
    temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    info!(
        "Creating temporary file {} for distance matrix",
        temp_filepath.to_string_lossy()
    );
    let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
        &graph,
        sim.observable_parameters.max_distance,
        &dmatrix_file,
    )?;

    let ddist_dataset = dmatrix.dataset(distance_matrix::DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let ddist = ddist_dataset.read_1d::<u64>()?;
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u64>()?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let ddist_dataset = datafile.dataset(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset.write_slice(&ddist, s![i, ..])?;
    let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    datafile.close()?;

    debug!("Sampling point to calculate the observables for");
    let sample_iter = std::iter::repeat_with(|| graph.sample_node(&mut sim.rng));
    let samples = Array1::from_iter(sample_iter.take(SAMPLE_SIZE).map(usize::from));
    debug!("Obtained samples: {}", samples.slice(s![0usize..10]));
    warn!("Writing samples to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(SAMPLES_DATASET_NAME)?;
    dataset.write_slice(&samples, s![i, ..])?;
    datafile.close()?;

    let max_delta = sim.observable_parameters.max_delta;
    info!("Measuring {SAMPLE_SIZE} samples of the average sphere profile on the graph");
    let deltas: Vec<usize> = (1..max_delta + 1).collect();

    use average_sphere_distance::single_sphere::profile_points_full_graph_from_disk as asd_profile;
    let asd = asd_profile::<u8, u32, _>(&dmatrix, samples.view(), deltas.iter().copied());
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_DATASET_NAME)?;
    dataset.write_slice(&asd, s![i, .., ..])?;
    datafile.close()?;

    info!("Measuring {SAMPLE_SIZE} samples of the average sphere profile the old way on the graph");
    use average_sphere_distance::single_sphere::profile_sized_graph as asd_profile_simple;
    let mut asd_profile = Array2::zeros([deltas.len(), 0]);
    for &sample in samples.iter() {
        let point = Label::from(sample);
        let asd = asd_profile_simple(&graph, point, deltas.iter().copied());
        asd_profile.push_column(ArrayView1::from(&asd))?;
    }
    let profile = asd_profile.as_standard_layout();
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_SIMPLE_DATASET_NAME)?;
    dataset.write_slice(&profile, s![i, .., ..])?;
    datafile.close()?;

    // info!(
    //     "Measuring full graph of the average sphere profile for deltas: {}",
    //     CORRELATION_DELTAS.clone().collect::<Array1<usize>>()
    // );
    // use average_sphere_distance::single_sphere::profile_full_graph_from_disk as full_asd_profile;
    // let asd = full_asd_profile::<u8, u32, _>(&dmatrix, CORRELATION_DELTAS);
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let dataset = datafile.dataset(ASD_FULL_DATASET_NAME)?;
    // dataset.write_slice(&asd, s![i, .., ..])?;
    // datafile.close()?;

    info!("Measuring vertex degree distributions on the graph");
    let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;
    let nbr_degree_field = vertex_degree_distribution::average_neighbour_field_from_disk(&dmatrix)?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let degree_dataset = datafile.dataset(DEGREE_DATASET_NAME)?;
    degree_dataset.write_slice(&degree_field, s![i, ..])?;
    let nbr_dataset = datafile.dataset(NBR_DEGREE_DATASET_NAME)?;
    nbr_dataset.write_slice(&nbr_degree_field, s![i, ..])?;
    datafile.close()?;

    // // Two point functions
    // let max_correlation_distance = sim.observable_parameters.max_correlation_distance;
    // let correlation_distances = 1..max_correlation_distance + 1;
    // info!("Sampling correlation pairs uniformly");
    // let (two_point_samples0, two_point_samples1) =
    //     two_point_function::sample_uniform_full_graph_from_disk(
    //         &dmatrix,
    //         correlation_distances.clone(),
    //         TWO_POINT_SAMPLE_SIZE,
    //         &mut sim.rng,
    //     )?;
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let samples2p_dataset = datafile.dataset(SAMPLES2P_UNIFORM_DATASET_NAME)?;
    // samples2p_dataset.write_slice(&two_point_samples0, s![0, i, .., ..])?;
    // samples2p_dataset.write_slice(&two_point_samples1, s![1, i, .., ..])?;
    // datafile.close()?;

    // info!("Sampling correlation pairs from origin");
    // let (two_point_samples0, two_point_samples1) =
    //     two_point_function::sample_from_origin_full_graph_from_disk(
    //         &dmatrix,
    //         correlation_distances,
    //         TWO_POINT_SAMPLE_SIZE,
    //         &mut sim.rng,
    //     )?;
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let samples2p_dataset = datafile.dataset(SAMPLES2P_ORIGIN_DATASET_NAME)?;
    // samples2p_dataset.write_slice(&two_point_samples0, s![0, i, .., ..])?;
    // samples2p_dataset.write_slice(&two_point_samples1, s![1, i, .., ..])?;
    // datafile.close()?;

    info!("Finished observable measurements");
    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    dmatrix_file.close()?;

    Ok(())
}
