use log::{debug, error};

use super::Parameters;
use std::path::PathBuf;

pub mod asd_violations;
pub mod double_sphere;
pub mod full_graph;
pub mod full_graph_sampled;
pub mod random_walk;
pub mod sampled_graph;
pub mod sphere_volumes;
pub mod two_point_sampled;
pub mod volume_profile;

/// Factor determining effective spatial radius from average timeslice length
const EFFECTIVE_RADIUS_SCALE: f32 = 0.144;
/// Factor for determining the maximum distance used based on the effective radius,
/// should be somewhere above 1 to make sure the effective radius is reached, and below
/// 2 to avoid to many unnecessary points.
const MAX_DISTANCE_FROM_EFF_RADIUS_SCALE: f32 = 1.4;
// const FIXED_MAX_DISTANCE: usize = 150;
// const MAX_DIFFUSION_TIME_FACTOR: f32 = 0.17;
const MAX_DIFFUSION_TIME: usize = 30_000;
const FIXED_MAX_DELTA: usize = 18;
// const MAX_DIFFUSION_TIME: usize = 250;

#[derive(Debug, Clone)]
pub struct ObservableParameters {
    max_distance: usize,
    max_correlation_distance: usize,
    max_delta: usize,
    #[allow(dead_code)]
    max_diffusion_time: usize,
    temp_dir: PathBuf,
    pub observable_set: ObservableSet,
}

#[derive(Debug, Clone, Copy, clap::ValueEnum)]
pub enum ObservableSet {
    FullGraph,
    FullGraphSampled,
    TwoPointSampled,
    AsdViolations,
    SphereVolumes,
    SampledGraph,
    DoubleSphere,
    RandomWalk,
    VolumeProfile,
}

impl ObservableParameters {
    pub(super) fn initialize(
        parameters: &Parameters,
        temp_dir: PathBuf,
        observable_set: ObservableSet,
    ) -> Self {
        let average_slice_size = parameters.size / parameters.timeslices / 2;
        let effective_spatial_radius = EFFECTIVE_RADIUS_SCALE * (average_slice_size as f32);
        let effective_time_radius = parameters.timeslices as f32 / 2.0;
        let effective_radius = effective_spatial_radius.min(effective_time_radius);
        let max_distance = (effective_radius * MAX_DISTANCE_FROM_EFF_RADIUS_SCALE) as usize;
        if max_distance > u8::MAX as usize {
            error!(
                "Chosen max distance {max_distance} is larger than {} and the largest \
                distances are thus not stored correctly in the distance matrix",
                u8::MAX
            );
        }
        // let max_diffusion_time =
        //     (MAX_DIFFUSION_TIME_FACTOR * (parameters.size / 2) as f32) as usize;
        let max_diffusion_time = MAX_DIFFUSION_TIME;
        let max_delta = (max_distance / 3).min(FIXED_MAX_DELTA);
        // let max_distance = FIXED_MAX_DISTANCE;
        // debug!("Chosen max distance to be {max_distance} based on effective radius {effective_spatial_radius}");
        debug!("Chosen max distance to be {max_distance} with {max_diffusion_time} maximum diffusion time");

        Self {
            max_distance,
            max_correlation_distance: max_distance,
            max_delta,
            max_diffusion_time,
            temp_dir,
            observable_set,
        }
    }
}
