use std::str::FromStr;

use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

use dyntri::graph::{Graph, SampleGraph};
// use dyntri::collections::Label;
// use dyntri::graph::field::Field;
// use dyntri::graph::FieldGraph;
use dyntri::graph::{spacetime_graph::SpacetimeGraph, GenericGraph};
// use dyntri::observables::average_sphere_distance::single_sphere;
// use dyntri::observables::full_graph::distance_matrix;
// use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::two_point_function;
use dyntri::triangulation::FullTriangulation;
use hdf5::types::VarLenUnicode;
use log::{debug, info, warn};
use ndarray::prelude::*;

// const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
// const DEGREE_DISTRIBUTION_DATASET_NAME: &str = "vertex-degree-distribution";
// const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const TWO_POINT_DEGREE_DATASET_NAME: &str = "two-point-vertex-degree";
// const TWO_POINT_ASD_DATASET_NAME: &str = "two-point-average-sphere-distance";
// const TWO_POINT_SVOL_DATASET_NAME: &str = "two-point-sphere-volume";

/// A maximum vertex degree of 50 is very conservative, the highest is likely a lot lower
// const MAX_VERTEX_DEGREE: usize = 50;
const TWO_POINT_SAMPLE_SIZE: usize = 2000;

fn create_hdf5_string_array<S: AsRef<str>>(
    list: &[S],
) -> Result<Array1<VarLenUnicode>, <VarLenUnicode as FromStr>::Err> {
    list.iter()
        .map(|s| VarLenUnicode::from_str(s.as_ref()))
        .collect()
}

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    // debug!("Preparing vertex degree distribution dataset");
    // let dataset = file
    //     .new_dataset::<u32>()
    //     .shape([parameters.measurements, MAX_VERTEX_DEGREE])
    //     .blosc_lz4(9, true)
    //     .create(DEGREE_DISTRIBUTION_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(Array1::from_iter(0..MAX_VERTEX_DEGREE).view())
    //     .create("degree")?;

    // debug!("Preparing average sphere volume dataset");
    // let dataset = file
    //     .new_dataset::<f32>()
    //     .shape([2, parameters.measurements, obs_parameters.max_distance + 1])
    //     .blosc_lz4(9, true)
    //     .create(SPHERE_VOLUMES_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(
    //         create_hdf5_string_array(&["statistic", "triangulation", "distance"])
    //             .expect("In code constructed string should always convert")
    //             .view(),
    //     )
    //     .create("axes")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data::<'_, _, VarLenUnicode, _>(
    //         create_hdf5_string_array(&["mean", "std"])
    //             .expect("In code constructed string should always convert")
    //             .view(),
    //     )
    //     .create("statistic")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
    //     .create("radius")?;

    // debug!("Preparing two-point sphere volume dataset");
    // let dataset = file
    //     .new_dataset::<u16>()
    //     .shape([
    //         2,
    //         parameters.measurements,
    //         TWO_POINT_SAMPLE_SIZE,
    //         obs_parameters.max_correlation_distance + 1,
    //         obs_parameters.max_distance + 1,
    //     ])
    //     .chunk([
    //         1,
    //         1,
    //         TWO_POINT_SAMPLE_SIZE,
    //         obs_parameters.max_correlation_distance + 1,
    //         obs_parameters.max_distance + 1,
    //     ])
    //     .blosc_lz4(9, true)
    //     .create(TWO_POINT_SVOL_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(
    //         create_hdf5_string_array(&[
    //             "two-point",
    //             "triangulation",
    //             "sample",
    //             "distance",
    //             "radius",
    //         ])
    //         .expect("In code constructed strings should always convert")
    //         .view(),
    //     )
    //     .create("axes")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(
    //         0..obs_parameters.max_correlation_distance + 1,
    //     ))
    //     .create("distance")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
    //     .create("radius")?;

    debug!("Preparing two-point vertex degree dataset");
    let dataset = file
        .new_dataset::<u8>()
        .shape([
            2,
            parameters.measurements,
            TWO_POINT_SAMPLE_SIZE,
            obs_parameters.max_correlation_distance + 1,
        ])
        .chunk([
            1,
            1,
            TWO_POINT_SAMPLE_SIZE,
            obs_parameters.max_correlation_distance + 1,
        ])
        .blosc_lz4(9, true)
        .create(TWO_POINT_DEGREE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(
            create_hdf5_string_array(&["two-point", "triangulation", "sample", "distance"])
                .expect("In code constructed string should always convert")
                .view(),
        )
        .create("axes")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            0..obs_parameters.max_correlation_distance + 1,
        ))
        .create("distance")?;

    // debug!("Preparing two-point average sphere distance dataset");
    // let dataset = file
    //     .new_dataset::<f64>()
    //     .shape([
    //         2,
    //         parameters.measurements,
    //         TWO_POINT_SAMPLE_SIZE,
    //         obs_parameters.max_correlation_distance + 1,
    //         obs_parameters.max_delta,
    //     ])
    //     .chunk([
    //         1,
    //         1,
    //         TWO_POINT_SAMPLE_SIZE,
    //         obs_parameters.max_correlation_distance + 1,
    //         obs_parameters.max_delta,
    //     ])
    //     .blosc_lz4(9, true)
    //     .create(TWO_POINT_ASD_DATASET_NAME)?;
    // trace!("Adding attributes to two-point average sphere distance dataset");
    // dataset
    //     .new_attr_builder()
    //     .with_data(
    //         create_hdf5_string_array(&[
    //             "two-point",
    //             "triangulation",
    //             "sample",
    //             "distance",
    //             "delta",
    //         ])
    //         .expect("In code constructed strings should always convert")
    //         .view(),
    //     )
    //     .create("axes")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(
    //         0..obs_parameters.max_correlation_distance + 1,
    //     ))
    //     .create("distance")?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
    //     .create("delta")?;

    debug!("Finished setting up the HDF5 files");
    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let graph = GenericGraph::from_spacetime_graph(&spacetime_graph);

    // // Computing distance matrix
    // let mut temp_filepath = sim.observable_parameters.temp_dir.clone();
    // debug!("Temporary directory used is {:?}", &temp_filepath);
    // temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    // info!(
    //     "Creating temporary file {} for distance matrix",
    //     temp_filepath.to_string_lossy()
    // );
    // let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    // let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
    //     &graph,
    //     sim.observable_parameters.max_distance,
    //     &dmatrix_file,
    // )?;

    // // Average sphere volume
    // let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    // let sphere_volumes = sphere_vol_dataset.read_2d::<f32>()?;
    // let svol_mean = sphere_volumes
    //     .mean_axis(Axis(1))
    //     .expect("Axis 1 should not have vertex_count length not 0");
    // let svol_std = sphere_volumes.std_axis(Axis(1), 1.0);

    // warn!("Writing sphere volumes to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    // sphere_vol_dataset.write_slice(&svol_mean, s![0, i, ..])?;
    // sphere_vol_dataset.write_slice(&svol_std, s![1, i, ..])?;
    // datafile.close()?;

    // Two point samples
    let max_correlation_distance = sim.observable_parameters.max_correlation_distance;
    // let correlation_distances = 0..max_correlation_distance + 1;
    // info!(
    //     "Sampling {} correlation pairs uniformly up to distance {}",
    //     TWO_POINT_SAMPLE_SIZE, max_correlation_distance
    // );
    // let (two_point_samples0, two_point_samples1) =
    //     two_point_function::sample_from_origin_full_graph_from_disk(
    //         &dmatrix,
    //         correlation_distances,
    //         TWO_POINT_SAMPLE_SIZE,
    //         &mut sim.rng,
    //     )?;
    // let two_point_samples0 = two_point_samples0.reversed_axes();
    // let two_point_samples1 = two_point_samples1.reversed_axes();

    // // Track the index where the sample will be stored
    // let mut sample_index = graph.new_field();
    // let mut index: usize = 0;
    // // List of the sample labels where each index in `sample_index` point to the sample in this list
    // let samples: Array1<usize> = two_point_samples0
    //     .iter()
    //     .chain(two_point_samples1.iter())
    //     .copied()
    //     .filter(|&point| {
    //         let unset = sample_index.set(Label::from(point), index);
    //         if unset {
    //             index += 1;
    //         }
    //         unset
    //     })
    //     .collect();

    // // Sphere volumes two-point function
    // debug!("Determining sphere volume two point functions on the graph");
    // let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    // let sphere_volumes = sphere_vol_dataset.read_2d::<u16>()?;
    // let svol_sample0 = two_point_samples0
    //     .iter()
    //     .flat_map(|&ilabel| sphere_volumes.column(ilabel).into_iter().copied())
    //     .collect::<Array1<u16>>()
    //     .into_shape([
    //         two_point_samples0.shape()[0],
    //         two_point_samples0.shape()[1],
    //         sphere_volumes.shape()[0],
    //     ])
    //     .expect("Shape should be correct by construction");
    // let svol_sample1 = two_point_samples1
    //     .iter()
    //     .flat_map(|&ilabel| sphere_volumes.column(ilabel).into_iter().copied())
    //     .collect::<Array1<u16>>()
    //     .into_shape([
    //         two_point_samples1.shape()[0],
    //         two_point_samples1.shape()[1],
    //         sphere_volumes.shape()[0],
    //     ])
    //     .expect("Shape should be correct by construction");
    // warn!("Writing sphere volume two point functions to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let twopoint_svol_dataset = datafile.dataset(TWO_POINT_SVOL_DATASET_NAME)?;
    // twopoint_svol_dataset.write_slice(&svol_sample0, s![0, i, .., .., ..])?;
    // twopoint_svol_dataset.write_slice(&svol_sample1, s![1, i, .., .., ..])?;
    // datafile.close()?;

    // Measure vertex degrees
    // info!("Measuring vertex degree field on the graph");
    // let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;

    // let mut degree_distribution: Array1<u32> = Array1::zeros([MAX_VERTEX_DEGREE]);
    // for &degree in degree_field.iter() {
    //     degree_distribution[degree] += 1;
    // }
    // warn!("Writing degree distribution to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let degree_dataset = datafile.dataset(DEGREE_DISTRIBUTION_DATASET_NAME)?;
    // degree_dataset.write_slice(&degree_distribution, s![i, ..])?;
    // datafile.close()?;

    debug!("Determining vertex degree two point functions on the graph");
    let mut degree_sample0 = Array2::zeros([0, max_correlation_distance + 1]);
    let mut degree_sample1 = Array2::zeros([0, max_correlation_distance + 1]);
    for _ in 0..TWO_POINT_SAMPLE_SIZE {
        let origin = graph.sample_node(&mut sim.rng);
        let two_point_vdegree = two_point_function::measure(
            &graph,
            origin,
            |graph, label| graph.get_vertex_degree(label),
            max_correlation_distance,
            &mut sim.rng,
        );
        degree_sample0.push_row(
            Array1::from_elem([max_correlation_distance + 1], two_point_vdegree.origin).view(),
        )?;
        degree_sample1.push_row(Array1::from_vec(two_point_vdegree.profile).view())?;
    }

    // let degree_sample0: Array2<u8> = two_point_samples0.mapv(|ilabel| degree_field[ilabel] as u8);
    // let degree_sample1: Array2<u8> = two_point_samples1.mapv(|ilabel| degree_field[ilabel] as u8);

    warn!("Writing vertex degree two point function to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let twopoint_degree_dataset = datafile.dataset(TWO_POINT_DEGREE_DATASET_NAME)?;
    twopoint_degree_dataset.write_slice(&degree_sample0.as_standard_layout(), s![0, i, .., ..])?;
    twopoint_degree_dataset.write_slice(&degree_sample1.as_standard_layout(), s![1, i, .., ..])?;
    datafile.close()?;

    // // Average sphere distance
    // let max_delta = sim.observable_parameters.max_delta;
    // let deltas = 1..max_delta + 1;
    // let asd_profile = single_sphere::profile_points_full_graph_from_disk::<u16, u32, _>(
    //     &dmatrix,
    //     samples.view(),
    //     deltas,
    // );
    // let asd_sample0 = two_point_samples0
    //     .iter()
    //     .flat_map(|&ilabel| {
    //         let index = sample_index
    //             .get(Label::from(ilabel))
    //             .expect("label must exist by construction");
    //         asd_profile.column(index).into_iter().copied()
    //     })
    //     .collect::<Array1<f64>>()
    //     .into_shape([
    //         two_point_samples0.shape()[0],
    //         two_point_samples0.shape()[1],
    //         asd_profile.shape()[0],
    //     ])
    //     .expect("Shape should be correct by construction");
    // let asd_sample1 = two_point_samples1
    //     .iter()
    //     .flat_map(|&ilabel| {
    //         let index = sample_index
    //             .get(Label::from(ilabel))
    //             .expect("label must exist by construction");
    //         asd_profile.column(index).into_iter().copied()
    //     })
    //     .collect::<Array1<f64>>()
    //     .into_shape([
    //         two_point_samples1.shape()[0],
    //         two_point_samples1.shape()[1],
    //         asd_profile.shape()[0],
    //     ])
    //     .expect("Shape should be correct by construction");
    //
    // warn!("Writing ASD two point functions to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let twopoint_asd_dataset = datafile.dataset(TWO_POINT_ASD_DATASET_NAME)?;
    // twopoint_asd_dataset.write_slice(&asd_sample0, s![0, i, .., .., ..])?;
    // twopoint_asd_dataset.write_slice(&asd_sample1, s![1, i, .., .., ..])?;
    // datafile.close()?;

    // Clean up
    info!("Finished observable measurements");
    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    // dmatrix_file.close()?;

    Ok(())
}
