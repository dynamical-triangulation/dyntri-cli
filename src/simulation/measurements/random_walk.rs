use dyntri::graph::{spacetime_graph::SpacetimeGraph, GenericGraph};
use dyntri::graph::{Graph, IterSizedGraph, SampleGraph};
use dyntri::observables::random_walk;
use dyntri::triangulation::FullTriangulation;
use log::{info, warn};
use ndarray::{arr0, s, Array2, Axis};

use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

const RANDOM_WALK_DATASET_NAME: &str = "random-walk-distance-profile";
const MAX_WALKER_STEP: usize = 800;
const WALKER_SAMPLES: usize = 100;
const TRIANGULATION_SAMPLES: usize = 20;

pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let parameters = &simulation.parameters;

    let dataset = file
        .new_dataset::<f32>()
        .shape([
            parameters.measurements,
            TRIANGULATION_SAMPLES,
            MAX_WALKER_STEP + 1,
        ])
        .chunk([1, TRIANGULATION_SAMPLES, MAX_WALKER_STEP + 1])
        .blosc_lz4(9, true)
        .create(RANDOM_WALK_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(0))
        .create("sigma-start")?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(MAX_WALKER_STEP))
        .create("sigma-max")?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let periodic_graph = SpacetimeGraph::from_triangulation_dual(&full_triangulation);
    let graph = GenericGraph::from_spacetime_graph(&periodic_graph);
    for label in graph.iter() {
        assert_eq!(graph.get_vertex_degree(label), 3)
    }

    let mut walker_dprofile = Array2::zeros([0, MAX_WALKER_STEP + 1]);
    for _ in 0..TRIANGULATION_SAMPLES {
        let point = graph.sample_node(&mut sim.rng);
        let walker_distances =
            random_walk::measure(&graph, point, MAX_WALKER_STEP, WALKER_SAMPLES, &mut sim.rng);
        let walker_distance = walker_distances
            .map(|&r| r as f64 * r as f64)
            .mean_axis(Axis(0))
            .expect("Array is not 0 size by construction");
        walker_dprofile
            .push_row(walker_distance.view())
            .expect("Shape correct by construction");
    }

    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let walker_dataset = datafile.dataset(RANDOM_WALK_DATASET_NAME)?;
    walker_dataset.write_slice(&walker_dprofile, s![i, .., ..])?;
    datafile.close()?;

    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;
    info!("Finished observable measurements");

    Ok(())
}
