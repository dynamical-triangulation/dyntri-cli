use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

use anyhow::Context;
use dyntri::graph::{spacetime_graph::SpacetimeGraph, GenericGraph};
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::{
    average_sphere_distance, two_point_function, vertex_degree_distribution,
};
use dyntri::triangulation::FullTriangulation;
use log::{debug, info, warn};
use ndarray::prelude::*;

const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
const ASD_UNIFORM_DATASET_NAME: &str = "average-sphere-distance-uniform";
const ASD_ORIGIN_DATASET_NAME: &str = "average-sphere-distance-origin";
// const ASD_DATASET_NAME: &str = "average-sphere-distance";
const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const ASD_SAMPLES_UNIFORM_DATASET_NAME: &str = "asd-samples-uniform";
const ASD_SAMPLES_ORIGIN_DATASET_NAME: &str = "asd-samples-origin";
const DEGREE_DATASET_NAME: &str = "vertex-degree-field";

const SAMPLE_SIZE: usize = 5000;
// const OLD_SAMPLE_SIZE: usize = 10;

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let vertex_count = simulation.parameters.size / 2;
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    debug!("Preparing average sphere distance sampled dataset");
    // Average sphere distance sampled uniform
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_UNIFORM_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    // Origin sampled
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_ORIGIN_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    // // Old sampling
    // let dataset = file
    //     .new_dataset::<f64>()
    //     .shape([
    //         parameters.measurements,
    //         obs_parameters.max_delta,
    //         OLD_SAMPLE_SIZE,
    //     ])
    //     .chunk([1, obs_parameters.max_delta, OLD_SAMPLE_SIZE])
    //     .blosc_lz4(9, true)
    //     .create(ASD_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
    //     .create("delta")?;

    debug!("Preparing vertex degree field dataset");
    // Vertex degree field
    file.new_dataset::<u16>()
        .shape([parameters.measurements, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(DEGREE_DATASET_NAME)?;

    debug!("Preparing pair distance distribution dataset");
    // Pair distance distribution
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.measurements, obs_parameters.max_distance + 1])
        .chunk([1, obs_parameters.max_distance + 1])
        .blosc_lz4(9, true)
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    debug!("Preparing sphere volume dataset");
    // Sphere volumes
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            parameters.measurements,
            obs_parameters.max_distance + 1,
            vertex_count,
        ])
        .chunk([1, obs_parameters.max_distance + 1, vertex_count])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    debug!("Preparing ASD samples uniform dataset");
    // ASD samples uniform
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            2,
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, 1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_SAMPLES_UNIFORM_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    debug!("Preparing ASD samples origin dataset");
    // ASD samples origin
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            2,
            parameters.measurements,
            obs_parameters.max_delta,
            SAMPLE_SIZE,
        ])
        .chunk([1, 1, obs_parameters.max_delta, SAMPLE_SIZE])
        .blosc_lz4(9, true)
        .create(ASD_SAMPLES_ORIGIN_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> anyhow::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    // let periodic_graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let graph = GenericGraph::from_spacetime_graph(&spacetime_graph);

    let mut temp_filepath = sim.observable_parameters.temp_dir.clone();
    debug!("Temporary directory used is {:?}", &temp_filepath);
    temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    info!(
        "Creating temporary file {} for distance matrix",
        temp_filepath.to_string_lossy()
    );
    let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
        &graph,
        sim.observable_parameters.max_distance,
        &dmatrix_file,
    )?;

    let ddist_dataset = dmatrix.dataset(distance_matrix::DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let ddist = ddist_dataset.read_1d::<u64>()?;
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u64>()?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let ddist_dataset = datafile.dataset(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset.write_slice(&ddist, s![i, ..])?;
    let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    datafile.close()?;

    // Average sphere distance
    let max_delta = sim.observable_parameters.max_delta;
    info!("Measuring {SAMPLE_SIZE} samples of the average sphere profile on the graph");
    let deltas: Vec<usize> = (1..max_delta + 1).collect();

    use average_sphere_distance::double_sphere::measure_points_full_graph_from_disk as measure_asd;
    // ASD samples uniform
    info!("Sampling ASD pairs uniformly");
    let (asd_samples0, asd_samples1) = two_point_function::sample_uniform_full_graph_from_disk(
        &dmatrix,
        deltas.iter().copied(),
        SAMPLE_SIZE,
        &mut sim.rng,
    )?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let asd_samples_dataset = datafile.dataset(ASD_SAMPLES_UNIFORM_DATASET_NAME)?;
    asd_samples_dataset.write_slice(&asd_samples0, s![0, i, .., ..])?;
    asd_samples_dataset.write_slice(&asd_samples1, s![1, i, .., ..])?;
    datafile.close()?;

    info!("Measuring ASD pairs uniformly");
    let mut asd_profile = Array2::zeros([0, SAMPLE_SIZE]);
    for (i, &delta) in deltas.iter().enumerate() {
        let asd = measure_asd::<u8, u32>(
            &dmatrix,
            (asd_samples0.slice(s![i, ..]), asd_samples1.slice(s![i, ..])),
            delta,
        );
        asd_profile
            .push_row(asd.view())
            .context("Adding ASD delta onto profile")?;
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_UNIFORM_DATASET_NAME)?;
    dataset.write_slice(&asd_profile, s![i, .., ..])?;
    datafile.close()?;

    // ASD samples from origin
    info!("Sampling ASD pairs from origin");
    let (asd_samples0, asd_samples1) = two_point_function::sample_from_origin_full_graph_from_disk(
        &dmatrix,
        deltas.iter().copied(),
        SAMPLE_SIZE,
        &mut sim.rng,
    )?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let asd_samples_dataset = datafile.dataset(ASD_SAMPLES_ORIGIN_DATASET_NAME)?;
    asd_samples_dataset.write_slice(&asd_samples0, s![0, i, .., ..])?;
    asd_samples_dataset.write_slice(&asd_samples1, s![1, i, .., ..])?;
    datafile.close()?;

    info!("Measuring ASD pairs from origin");
    let mut asd_profile = Array2::zeros([0, SAMPLE_SIZE]);
    for (i, &delta) in deltas.iter().enumerate() {
        let asd = measure_asd::<u8, u32>(
            &dmatrix,
            (asd_samples0.slice(s![i, ..]), asd_samples1.slice(s![i, ..])),
            delta,
        );
        asd_profile
            .push_row(asd.view())
            .context("Adding ASD delta onto profile")?;
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_ORIGIN_DATASET_NAME)?;
    dataset.write_slice(&asd_profile, s![i, .., ..])?;
    datafile.close()?;

    // info!("Measuring double sphere ASD for a periodic graph in the old way");
    // let mut asds = Array2::zeros([deltas.len(), 0]);
    // for i in 0..OLD_SAMPLE_SIZE {
    //     debug!("Calculating ASD on periodic graph, sample {i}");
    //     let origin = periodic_graph.sample_node(&mut sim.rng);
    //     let asd = average_sphere_distance::double_sphere::profile(
    //         &periodic_graph,
    //         origin,
    //         deltas.iter().copied(),
    //         &mut sim.rng,
    //     );
    //     asds.push_column(ArrayView1::from(&asd.profile))
    //         .context("Adding ASD delta onto profile")?;
    // }
    // let asd_data = asds.as_standard_layout();
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = sim.open_datafile()?;
    // let dataset = datafile.dataset(ASD_DATASET_NAME)?;
    // dataset.write_slice(&asd_data, s![i, .., ..])?;
    // datafile.close()?;

    info!("Measuring vertex degree distributions on the graph");
    let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let degree_dataset = datafile.dataset(DEGREE_DATASET_NAME)?;
    degree_dataset.write_slice(&degree_field, s![i, ..])?;
    datafile.close()?;

    info!("Finished observable measurements");
    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    Ok(())
}
