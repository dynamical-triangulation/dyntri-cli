use crate::simulation::{Simulation, CURRENT_MEASUREMENT_ATTR_NAME};

use dyntri::graph::{spacetime_graph::SpacetimeGraph, GenericGraph};
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::{
    average_sphere_distance, two_point_function, vertex_degree_distribution,
};
use dyntri::triangulation::FullTriangulation;
use log::{debug, info, warn};
use ndarray::prelude::*;

const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
const ASD_DATASET_NAME: &str = "average-sphere-distance";
const DEGREE_DATASET_NAME: &str = "vertex-degree-field";
const NBR_DEGREE_DATASET_NAME: &str = "average-neighbour-degree-field";
const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const ASD_CORR_DATASET_NAME: &str = "asd-two-point-function";
const VERTEX_DEGREE_CORR_DATASET_NAME: &str = "vertex-degree-two-point-function";

const CORRELATION_DELTAS: [usize; 5] = [5, 8, 10, 12, 15];

/// Initialize the dataset for each observable
pub fn initialize_observables(file: &hdf5::File, simulation: &Simulation) -> hdf5::Result<()> {
    let vertex_count = simulation.parameters.size / 2;
    let parameters = &simulation.parameters;
    let obs_parameters = &simulation.observable_parameters;

    // Average sphere distance
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_delta,
            vertex_count,
        ])
        .chunk([1, obs_parameters.max_delta, vertex_count])
        .blosc_lz4(9, true)
        .create(ASD_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data("average-sphere-distance")
        .create("type")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..obs_parameters.max_delta + 1))
        .create("delta")?;

    // Vertex degree field
    file.new_dataset::<u16>()
        .shape([parameters.measurements, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(DEGREE_DATASET_NAME)?;

    // Average neighbour vertex degree field
    file.new_dataset::<f32>()
        .shape([parameters.measurements, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(NBR_DEGREE_DATASET_NAME)?;

    // Pair distance distribution
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.measurements, obs_parameters.max_distance + 1])
        .chunk([1, obs_parameters.max_distance + 1])
        .blosc_lz4(9, true)
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    // Sphere volumes
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            parameters.measurements,
            obs_parameters.max_distance + 1,
            vertex_count,
        ])
        .chunk([1, obs_parameters.max_distance + 1, vertex_count])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..obs_parameters.max_distance + 1))
        .create("r")?;

    // ASD correlation
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            CORRELATION_DELTAS.len(),
            obs_parameters.max_correlation_distance,
        ])
        .chunk([
            1,
            CORRELATION_DELTAS.len(),
            obs_parameters.max_correlation_distance,
        ])
        .blosc_lz4(9, true)
        .create(ASD_CORR_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&CORRELATION_DELTAS)
        .create("delta")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            1..obs_parameters.max_correlation_distance + 1,
        ))
        .create("r")?;

    // Vertex degree correlation
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            parameters.measurements,
            obs_parameters.max_correlation_distance,
        ])
        .chunk([1, obs_parameters.max_correlation_distance])
        .blosc_lz4(9, true)
        .create(VERTEX_DEGREE_CORR_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            1..obs_parameters.max_correlation_distance + 1,
        ))
        .create("r")?;
    Ok(())
}

pub fn perform_measurements(sim: &mut Simulation, i: usize) -> hdf5::Result<()> {
    info!("Performing observable measurements");
    let full_triangulation = FullTriangulation::from_triangulation(&sim.cdt.triangulation);
    info!("Constructing graph from triangulation");
    let spacetime_graph = SpacetimeGraph::from_triangulation_vertex(&full_triangulation);
    let graph = GenericGraph::from_spacetime_graph(&spacetime_graph);

    let mut temp_filepath = sim.observable_parameters.temp_dir.clone();
    debug!("Temporary directory used is {:?}", &temp_filepath);
    temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    info!(
        "Creating temporary file {} for distance matrix",
        temp_filepath.to_string_lossy()
    );
    let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
        &graph,
        sim.observable_parameters.max_distance,
        &dmatrix_file,
    )?;

    let ddist_dataset = dmatrix.dataset(distance_matrix::DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let ddist = ddist_dataset.read_1d::<u64>()?;
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u64>()?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let ddist_dataset = datafile.dataset(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset.write_slice(&ddist, s![i, ..])?;
    let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    datafile.close()?;

    let max_delta = sim.observable_parameters.max_delta;
    info!("Measuring the average sphere profile on the graph");
    let deltas: Vec<usize> = (1..max_delta + 1).collect();
    let asd = average_sphere_distance::single_sphere::profile_full_graph_from_disk::<u8, u32, _>(
        &dmatrix,
        deltas.iter().copied(),
    );
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let dataset = datafile.dataset(ASD_DATASET_NAME)?;
    dataset.write_slice(&asd, s![i, .., ..])?;
    datafile.close()?;

    let max_correlation_distance = sim.observable_parameters.max_correlation_distance;
    info!("Measuring average sphere distance correlations");
    let mut two_point_function = Array2::zeros([0, max_correlation_distance]);
    for &delta in CORRELATION_DELTAS.iter() {
        if delta > max_delta {
            two_point_function
                .push_row(Array1::zeros(max_correlation_distance).view())
                .unwrap_or_else(|_| {
                    panic!(
                        "Shape {:?} into {:?} should be correct by construction",
                        max_correlation_distance,
                        two_point_function.shape()
                    )
                });
            continue;
        }
        let idelta = deltas
            .iter()
            .copied()
            .enumerate()
            .find(|&(_, list_delta)| list_delta == delta)
            .expect("Chosen delta should be in `deltas` list")
            .0;
        let two_point_delta = two_point_function::measure_full_graph_from_disk(
            &dmatrix,
            asd.slice(s![idelta, ..]),
            &|obs0, obs1| obs0 * obs1,
            1..max_correlation_distance + 1,
        )?;
        two_point_function
            .push_row(two_point_delta.view())
            .unwrap_or_else(|_| {
                panic!(
                    "Shape {:?} into {:?} should be correct by construction",
                    two_point_delta.shape(),
                    two_point_function.shape()
                )
            });
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let asd_two_point_dataset = datafile.dataset(ASD_CORR_DATASET_NAME)?;
    asd_two_point_dataset.write_slice(&two_point_function, s![i, .., ..])?;
    datafile.close()?;

    info!("Measuring vertex degree distributions on the graph");
    let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;
    let nbr_degree_field = vertex_degree_distribution::average_neighbour_field_from_disk(&dmatrix)?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let degree_dataset = datafile.dataset(DEGREE_DATASET_NAME)?;
    degree_dataset.write_slice(&degree_field, s![i, ..])?;
    let nbr_dataset = datafile.dataset(NBR_DEGREE_DATASET_NAME)?;
    nbr_dataset.write_slice(&nbr_degree_field, s![i, ..])?;
    datafile.close()?;

    info!("Measuring vertex degree correlations");
    let two_point_function = two_point_function::measure_full_graph_from_disk(
        &dmatrix,
        degree_field.view(),
        &|obs0, obs1| (obs0 * obs1) as f64,
        1..max_correlation_distance + 1,
    )?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = sim.open_datafile()?;
    let degree_two_point_dataset = datafile.dataset(VERTEX_DEGREE_CORR_DATASET_NAME)?;
    degree_two_point_dataset.write_slice(&two_point_function, s![i, ..])?;
    datafile.close()?;

    info!("Finished observable measurements");
    let datafile = sim.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    Ok(())
}
