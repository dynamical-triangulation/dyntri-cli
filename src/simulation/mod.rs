use dyntri::monte_carlo::fixed_universe::Universe;
use format_num::format_num;
use log::{debug, info};
use ndarray::prelude::*;
use rand::SeedableRng;
use rand_xoshiro::Xoshiro256StarStar;
use std::{path::PathBuf, time};

use crate::{
    config::{self, Config},
    simulation::measurements::{
        asd_violations, double_sphere, full_graph_sampled, random_walk, sampled_graph,
        sphere_volumes, two_point_sampled, volume_profile,
    },
};

use self::measurements::{full_graph, ObservableParameters};

mod measurements;
pub use self::measurements::ObservableSet;

pub const DEFAULT_OUTFILE_DIR: &str = "./";
pub const DEFAULT_MEASUREMENT_BASENAME: &str = "measurement";
const HDF5_FILE_EXTENSION: &str = "h5";
const CURRENT_MEASUREMENT_ATTR_NAME: &str = "current-measurement";
const SIMULATION_FINISHED_ATTR_NAME: &str = "simulation-finished";

pub fn run_cdt_simulation(config: PathBuf, temp_dir: PathBuf, observable_set: ObservableSet) {
    let config: Config = config::read_toml(config).unwrap();
    let rng = rand_xoshiro::Xoshiro256StarStar::from_entropy();

    let mut simulation = Simulation::new(&config, rng, temp_dir, observable_set);
    simulation.run();
}

#[derive(Debug, Clone)]
struct Parameters {
    size: usize,
    timeslices: usize,
    sweep: usize,
    measurements: usize,
    burnin: usize,
    interval: f32,
    output_filepath: PathBuf,
}

#[derive(Debug, Clone)]
pub struct Simulation {
    parameters: Parameters,
    observable_parameters: ObservableParameters,
    rng: Xoshiro256StarStar,
    cdt: Universe,
}

/// Create unique filename using timestamp and `system_size` being the amount of triangles
pub fn create_filename(system_size: usize) -> String {
    let timestamp = time::SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .expect("System time seems to be before UNIX EPOCH")
        .as_micros();
    let size_str = format_num!(".2s", system_size as f64);
    format!("{DEFAULT_MEASUREMENT_BASENAME}-{size_str}-{timestamp}")
}

fn create_filepath(config: &Config) -> PathBuf {
    let mut filepath = config.output_filepath.clone().unwrap_or_else(|| {
        let mut filepath = PathBuf::from(DEFAULT_OUTFILE_DIR);
        let filename = create_filename(config.size);
        filepath.set_file_name(filename);
        filepath
    });
    filepath.set_extension(HDF5_FILE_EXTENSION);
    filepath
}

impl Simulation {
    pub fn new(
        config: &Config,
        rng: Xoshiro256StarStar,
        temp_dir: PathBuf,
        observable_set: ObservableSet,
    ) -> Self {
        let cdt = Universe::default(config.size, config.timeslices);
        let output_filepath = create_filepath(config);
        debug!("Created filepath {:?} for measurements", output_filepath);
        let parameters = Parameters {
            size: cdt.size(),
            timeslices: config.timeslices,
            sweep: cdt.size(),
            measurements: config.measurements,
            burnin: config.burnin,
            interval: config.interval,
            output_filepath,
        };
        let observable_parameters =
            ObservableParameters::initialize(&parameters, temp_dir, observable_set);
        Self {
            parameters,
            rng,
            cdt,
            observable_parameters,
        }
    }

    /// Create and initlize attributes of HDF5 output file
    fn initialize_datafile(&self) -> hdf5::Result<()> {
        let parameters = &self.parameters;

        let file = hdf5::File::create_excl(&parameters.output_filepath)?;
        file.new_attr_builder()
            .with_data(&arr0(0))
            .create(CURRENT_MEASUREMENT_ATTR_NAME)?;
        file.new_attr_builder()
            .with_data(&arr0(false))
            .create(SIMULATION_FINISHED_ATTR_NAME)?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.size))
            .create("size")?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.timeslices))
            .create("timeslices")?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.measurements))
            .create("measurements")?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.sweep))
            .create("sweep")?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.burnin))
            .create("burnin")?;
        file.new_attr_builder()
            .with_data(&arr0(parameters.interval))
            .create("interval")?;

        self.initialize_observables(&file)?;

        file.close()
    }
}

impl Simulation {
    pub fn run(&mut self) {
        self.initialize_datafile()
            .expect("Could not initialize the HDF5 datafile");
        self.run_burnin();
        self.run_simulation();
    }

    fn run_burnin(&mut self) {
        info!("Equilibrating system");
        for _ in 0..self.parameters.burnin {
            for _ in 0..self.parameters.sweep {
                self.cdt.step(&mut self.rng);
            }
        }
        info!("Finished equilibration");
    }

    fn run_simulation(&mut self) {
        info!("Starting measurement phase");
        info!(
            "Simulation measurement step {}/{}",
            1, self.parameters.measurements
        );
        self.perform_measurements(0)
            .expect("Observable measurement could not use HDF5 correctly");
        for step in 1..self.parameters.measurements {
            info!(
                "Simulation measurement step {}/{}",
                step + 1,
                self.parameters.measurements
            );
            // Run for `interval` sweeps
            let interval_steps =
                (self.parameters.interval.abs() * self.parameters.sweep as f32) as usize;
            for _ in 0..interval_steps {
                self.cdt.step(&mut self.rng);
            }

            self.perform_measurements(step)
                .expect("Observable measurement went wrong");
        }

        info!("Finished simulation");
        if self
            .open_datafile()
            .and_then(|datafile| datafile.attr(SIMULATION_FINISHED_ATTR_NAME))
            .and_then(|sim_finished| sim_finished.write_scalar(&true))
            .is_err()
        {
            log::error!("Could not flag the simulation as finished in HDF5 file");
        }
    }

    fn perform_measurements(&mut self, step: usize) -> anyhow::Result<()> {
        use ObservableSet::*;
        match self.observable_parameters.observable_set {
            FullGraph => full_graph::perform_measurements(self, step)?,
            FullGraphSampled => full_graph_sampled::perform_measurements(self, step)?,
            TwoPointSampled => two_point_sampled::perform_measurements(self, step)?,
            SphereVolumes => sphere_volumes::perform_measurements(self, step)?,
            SampledGraph => sampled_graph::perform_measurements(self, step)?,
            DoubleSphere => double_sphere::perform_measurements(self, step)?,
            RandomWalk => random_walk::perform_measurements(self, step)?,
            VolumeProfile => volume_profile::perform_measurements(self, step)?,
            AsdViolations => asd_violations::perform_measurements(self, step)?,
        }

        anyhow::Ok(())
    }

    fn initialize_observables(&self, file: &hdf5::File) -> hdf5::Result<()> {
        use ObservableSet::*;
        match self.observable_parameters.observable_set {
            FullGraph => full_graph::initialize_observables(file, self)?,
            FullGraphSampled => full_graph_sampled::initialize_observables(file, self)?,
            TwoPointSampled => two_point_sampled::initialize_observables(file, self)?,
            SphereVolumes => sphere_volumes::initialize_observables(file, self)?,
            SampledGraph => sampled_graph::initialize_observables(file, self)?,
            DoubleSphere => double_sphere::initialize_observables(file, self)?,
            RandomWalk => random_walk::initialize_observables(file, self)?,
            VolumeProfile => volume_profile::initialize_observables(file, self)?,
            AsdViolations => asd_violations::initialize_observables(file, self)?,
        }

        Ok(())
    }

    fn open_datafile(&self) -> hdf5::Result<hdf5::File> {
        hdf5::File::open_rw(&self.parameters.output_filepath)
    }
}
