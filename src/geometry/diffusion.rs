use std::fs;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};

use anyhow::Context;
use dyntri::graph::{GenericGraph, Graph, SampleGraph};
use dyntri::io::graph::import_adjacency_list_thijs_patch;
use dyntri::observables::diffusion;
use log::{debug, info, warn};
use ndarray::prelude::*;
use rand::{Rng, SeedableRng};

// const DEGREE_DATASET_NAME: &str = "vertex-degree-field";
const DIFFUSION_DATASET_NAME: &str = "return-probability";

const CURRENT_MEASUREMENT_ATTR_NAME: &str = "current-measurement";
const SIZE_ATTR_NAME: &str = "graph-size";
const CONFIGURATIONS_ATTR_NAME: &str = "configurations";

const DIFFUSION_SAMPLE_SIZE: usize = 1;
const DIFFUSION_COEFFICIENT: f64 = 0.90;

const MAX_DIFFUSION_TIME: usize = 25_000;
// const EFFECTIVE_RADIUS_FACTOR: f32 = 1.56;
// const EFFECTIVE_DIMENSION: f32 = 4.0;
// const MAX_DISTANCE_FACTOR: f32 = 1.75;
// const MAX_DIFFUSION_TIME_FACTOR: f32 = 0.17;

struct Parameters {
    size: usize,
    configurations: usize,
    // max_distance: usize,
    max_diffusion_time: usize,
    // temp_dir: PathBuf,
    file_out: PathBuf,
}

impl Parameters {
    fn open_datafile(&self) -> hdf5::Result<hdf5::File> {
        hdf5::File::open_rw(&self.file_out)
    }
}

#[allow(unused_variables)]
pub fn start_measurement(file_out: &Path, dir_in: &Path, temp_dir: &Path) -> anyhow::Result<()> {
    info!("Loading files from directory {:?}", dir_in);
    let adj_filenames: Vec<PathBuf> = fs::read_dir(dir_in)?
        .map(|entry| entry.map(|e| e.path()))
        .collect::<Result<_, _>>()?;

    let size = get_size(
        adj_filenames
            .first()
            .with_context(|| "No files in directory")?,
    )?;
    // let effective_radius = EFFECTIVE_RADIUS_FACTOR * (size as f32).powf(1.0 / EFFECTIVE_DIMENSION);
    // let max_distance = (MAX_DISTANCE_FACTOR * effective_radius) as usize;
    // let max_diffusion_time = (MAX_DIFFUSION_TIME_FACTOR * size as f32) as usize;
    let max_diffusion_time = MAX_DIFFUSION_TIME;
    info!("Using graph of size {size} and max diffusion time of {max_diffusion_time}");
    let parameters = Parameters {
        size,
        configurations: adj_filenames.len(),
        // max_distance,
        max_diffusion_time,
        // temp_dir: temp_dir.to_path_buf(),
        file_out: file_out.to_path_buf(),
    };

    debug!("Creating datafile for measurement storage");
    let file = hdf5::File::create_excl(file_out)?;
    info!("Initializing attributes in output file");
    initialize_attributes(&file, &parameters)?;
    info!("Initializing observables in output file");
    initialize_observables(&file, &parameters)?;
    file.close()?;

    info!("Starting observable measurements");
    start_measurements(&adj_filenames, &parameters)
}

fn get_size(filename: &Path) -> anyhow::Result<usize> {
    let adj_file = fs::File::open(filename).with_context(|| "Could not open file")?;
    Ok(BufReader::new(adj_file).lines().count())
}

fn start_measurements(files: &[PathBuf], parameters: &Parameters) -> anyhow::Result<()> {
    let mut rng = rand_xoshiro::Xoshiro256PlusPlus::from_entropy();
    for (i, filename) in files.iter().enumerate() {
        info!(
            "Starting measurement {}/{}",
            i + 1,
            parameters.configurations
        );
        debug!("Loading graph from file {:?}", filename);
        let adj_file = std::fs::File::open(filename)?;
        let graph = load_graph_from_adjacency(adj_file)
            .with_context(|| "Could not load graph from file.")?;
        perform_measurements(&graph, parameters, &mut rng, i)?;
    }
    Ok(())
}

fn load_graph_from_adjacency(file: std::fs::File) -> anyhow::Result<GenericGraph> {
    // let nodes = import_adjacency_list(file)?;
    let nodes = import_adjacency_list_thijs_patch(file)?;
    Ok(GenericGraph::new(nodes))
}

fn initialize_attributes(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    file.new_attr_builder()
        .with_data(arr0(0).view())
        .create(CURRENT_MEASUREMENT_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.size).view())
        .create(SIZE_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.configurations).view())
        .create(CONFIGURATIONS_ATTR_NAME)?;

    Ok(())
}

/// Initialize the dataset for each observable
fn initialize_observables(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    // let vertex_count = parameters.size;

    // debug!("Preparing vertex degree field dataset");
    // // Vertex degree field
    // file.new_dataset::<u16>()
    //     .shape([parameters.configurations, vertex_count])
    //     .chunk([1, vertex_count])
    //     .blosc_lz4(9, true)
    //     .create(DEGREE_DATASET_NAME)?;

    debug!("Preparing diffusion dataset");
    let dataset = file
        .new_dataset::<f32>()
        .shape([
            parameters.configurations,
            DIFFUSION_SAMPLE_SIZE,
            parameters.max_diffusion_time + 1,
        ])
        .chunk([1, DIFFUSION_SAMPLE_SIZE, parameters.max_diffusion_time + 1])
        .blosc_lz4(9, true)
        .create(DIFFUSION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(0))
        .create("sigma-start")?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(parameters.max_diffusion_time))
        .create("sigma-max")?;

    Ok(())
}

fn perform_measurements<R: Rng + ?Sized>(
    graph: &GenericGraph,
    parameters: &Parameters,
    rng: &mut R,
    i: usize,
) -> anyhow::Result<()> {
    info!("Measuring vertex degree distributions on the graph");
    // let degree_field = vertex_degree_distribution::field_from_graph(graph);
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = parameters.open_datafile()?;
    // let degree_dataset = datafile.dataset(DEGREE_DATASET_NAME)?;
    // degree_dataset.write_slice(&degree_field, s![i, ..])?;
    // datafile.close()?;

    // Sample a set of points in the graph to use for diffusion measurements
    let points: Vec<<GenericGraph as Graph>::Label> = (0..DIFFUSION_SAMPLE_SIZE)
        .map(|_| graph.sample_node(rng))
        .collect();

    let max_diffusion_time = parameters.max_diffusion_time;
    let mut return_probability = Array2::zeros([0, max_diffusion_time + 1]);
    debug!("Measuring return probability on graph");
    for &point in points.iter() {
        let rprop = Array1::from_vec(
            diffusion::measure_sized(graph, point, max_diffusion_time + 1, DIFFUSION_COEFFICIENT).0,
        );
        return_probability
            .push_row(rprop.view())
            .expect("Shapes should be correct by construction")
    }
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let return_prob_dataset = datafile.dataset(DIFFUSION_DATASET_NAME)?;
    return_prob_dataset.write_slice(&return_probability, s![i, .., ..])?;
    datafile.close()?;

    info!("Finished observable measurements");
    let datafile = parameters.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    Ok(())
}
