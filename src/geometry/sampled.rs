use std::fs;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};

use anyhow::Context;
use dyntri::graph::GenericGraph;
use dyntri::io::graph::import_adjacency_list_thijs_patch;
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::{two_point_function, vertex_degree_distribution};
use log::{debug, info, warn};
use ndarray::prelude::*;
use rand::{Rng, SeedableRng};

const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
// const ASD_DATASET_NAME: &str = "average-sphere-distance";
// const ASD_FULL_DATASET_NAME: &str = "average-sphere-distance-full";
const DEGREE_DATASET_NAME: &str = "vertex-degree-field";
const NBR_DEGREE_DATASET_NAME: &str = "average-neighbour-degree-field";
const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const SAMPLES2P_UNIFORM_DATASET_NAME: &str = "two-point-samples-uniform";
const SAMPLES2P_ORIGIN_DATASET_NAME: &str = "two-point-samples-origin";
// const SAMPLES_DATASET_NAME: &str = "point-samples";

const CURRENT_MEASUREMENT_ATTR_NAME: &str = "current-measurement";
const SIZE_ATTR_NAME: &str = "graph-size";
const CONFIGURATIONS_ATTR_NAME: &str = "configurations";

// const CORRELATION_DELTAS: std::ops::Range<usize> = 8..15;
// const SAMPLE_SIZE: usize = 5000;
const TWO_POINT_SAMPLE_SIZE: usize = 10_000;

const EFFECTIVE_RADIUS_FACTOR: f32 = 1.56;
const EFFECTIVE_DIMENSION: f32 = 4.0;
const MAX_DISTANCE_FACTOR: f32 = 1.75;

struct Parameters {
    size: usize,
    configurations: usize,
    max_distance: usize,
    // max_delta: usize,
    max_correlation_distance: usize,
    temp_dir: PathBuf,
    file_out: PathBuf,
}

impl Parameters {
    fn open_datafile(&self) -> hdf5::Result<hdf5::File> {
        hdf5::File::open_rw(&self.file_out)
    }
}

pub fn start_measurement(file_out: &Path, dir_in: &Path, temp_dir: &Path) -> anyhow::Result<()> {
    info!("Loading files from directory {:?}", dir_in);
    let adj_filenames: Vec<PathBuf> = fs::read_dir(dir_in)?
        .map(|entry| entry.map(|e| e.path()))
        .collect::<Result<_, _>>()?;

    let size = get_size(
        adj_filenames
            .first()
            .with_context(|| "No files in directory")?,
    )?;
    let effective_radius = EFFECTIVE_RADIUS_FACTOR * (size as f32).powf(1.0 / EFFECTIVE_DIMENSION);
    let max_distance = (MAX_DISTANCE_FACTOR * effective_radius) as usize;
    info!("Using maximum distance of {max_distance}");
    let parameters = Parameters {
        size,
        configurations: adj_filenames.len(),
        max_distance,
        // max_delta: max_distance / 2,
        max_correlation_distance: max_distance,
        temp_dir: temp_dir.to_path_buf(),
        file_out: file_out.to_path_buf(),
    };

    debug!("Creating datafile for measurement storage");
    let file = hdf5::File::create_excl(file_out)?;
    info!("Initializing attributes in output file");
    initialize_attributes(&file, &parameters)?;
    info!("Initializing observales in output file");
    initialize_observables(&file, &parameters)?;
    file.close()?;

    info!("Starting observable measurements");
    start_measurements(&adj_filenames, &parameters)
}

fn get_size(filename: &Path) -> anyhow::Result<usize> {
    let adj_file = fs::File::open(filename).with_context(|| "Could not open file")?;
    Ok(BufReader::new(adj_file).lines().count())
}

fn start_measurements(files: &[PathBuf], parameters: &Parameters) -> anyhow::Result<()> {
    let mut rng = rand_xoshiro::Xoshiro256PlusPlus::from_entropy();
    for (i, filename) in files.iter().enumerate() {
        info!(
            "Starting measurement {}/{}",
            i + 1,
            parameters.configurations
        );
        debug!("Loading graph from file {:?}", filename);
        let adj_file = std::fs::File::open(filename)?;
        let graph = load_graph_from_adjacency(adj_file)
            .with_context(|| "Could not load graph from file.")?;
        perform_measurements(&graph, parameters, &mut rng, i)?;
    }
    Ok(())
}

fn load_graph_from_adjacency(file: std::fs::File) -> anyhow::Result<GenericGraph> {
    // let nodes = import_adjacency_list(file)?;
    let nodes = import_adjacency_list_thijs_patch(file)?;
    Ok(GenericGraph::new(nodes))
}

fn initialize_attributes(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    file.new_attr_builder()
        .with_data(arr0(0).view())
        .create(CURRENT_MEASUREMENT_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.size).view())
        .create(SIZE_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.configurations).view())
        .create(CONFIGURATIONS_ATTR_NAME)?;

    Ok(())
}

/// Initialize the dataset for each observable
fn initialize_observables(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    let vertex_count = parameters.size;

    // debug!("Preparing average sphere distance sampled dataset");
    // // Average sphere distance sampled
    // let dataset = file
    //     .new_dataset::<f64>()
    //     .shape([parameters.configurations, parameters.max_delta, SAMPLE_SIZE])
    //     .chunk([1, parameters.max_delta, SAMPLE_SIZE])
    //     .blosc_lz4(9, true)
    //     .create(ASD_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(1..parameters.max_delta + 1))
    //     .create("delta")?;

    // debug!("Preparing average sphere distance full graph dataset");
    // let cor_delta_amount = CORRELATION_DELTAS.len();
    // // Average sphere distance full graph
    // let dataset = file
    //     .new_dataset::<f64>()
    //     .shape([parameters.configurations, cor_delta_amount, vertex_count])
    //     .chunk([1, cor_delta_amount, vertex_count])
    //     .blosc_lz4(9, true)
    //     .create(ASD_FULL_DATASET_NAME)?;
    // dataset
    //     .new_attr_builder()
    //     .with_data(&Array1::from_iter(CORRELATION_DELTAS))
    //     .create("delta")?;

    debug!("Preparing vertex degree field dataset");
    // Vertex degree field
    file.new_dataset::<u16>()
        .shape([parameters.configurations, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(DEGREE_DATASET_NAME)?;

    debug!("Preparing average neighbour vertex degree field dataset");
    // Average neighbour vertex degree field
    file.new_dataset::<f32>()
        .shape([parameters.configurations, vertex_count])
        .chunk([1, vertex_count])
        .blosc_lz4(9, true)
        .create(NBR_DEGREE_DATASET_NAME)?;

    debug!("Preparing pair distance distribution dataset");
    // Pair distance distribution
    let dataset = file
        .new_dataset::<usize>()
        .shape([parameters.configurations, parameters.max_distance + 1])
        .chunk([1, parameters.max_distance + 1])
        .blosc_lz4(9, true)
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..parameters.max_distance + 1))
        .create("r")?;

    debug!("Preparing sphere volume dataset");
    // Sphere volumes
    let dataset = file
        .new_dataset::<usize>()
        .shape([
            parameters.configurations,
            parameters.max_distance + 1,
            vertex_count,
        ])
        .chunk([1, parameters.max_distance + 1, vertex_count])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..parameters.max_distance + 1))
        .create("r")?;

    // debug!("Preparing point sample dataset");
    // // Point samples
    // file.new_dataset::<usize>()
    //     .shape([parameters.configurations, SAMPLE_SIZE])
    //     .chunk([1, SAMPLE_SIZE])
    //     .blosc_lz4(9, true)
    //     .create(SAMPLES_DATASET_NAME)?;

    debug!("Preparing two-point sample uniform dataset");
    // Correlation/two-point samples uniform
    let dataset = file
        .new_dataset::<(usize, usize)>()
        .shape([
            parameters.configurations,
            parameters.max_correlation_distance,
            TWO_POINT_SAMPLE_SIZE,
        ])
        .chunk([
            1,
            parameters.max_correlation_distance,
            TWO_POINT_SAMPLE_SIZE,
        ])
        .blosc_lz4(9, true)
        .create(SAMPLES2P_UNIFORM_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            1..parameters.max_correlation_distance + 1,
        ))
        .create("r")?;

    debug!("Preparing two-point sample origin based dataset");
    // Correlation/two-point samples from origin
    let dataset = file
        .new_dataset::<(usize, usize)>()
        .shape([
            parameters.configurations,
            parameters.max_correlation_distance,
            TWO_POINT_SAMPLE_SIZE,
        ])
        .chunk([
            1,
            parameters.max_correlation_distance,
            TWO_POINT_SAMPLE_SIZE,
        ])
        .blosc_lz4(9, true)
        .create(SAMPLES2P_ORIGIN_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            1..parameters.max_correlation_distance + 1,
        ))
        .create("r")?;

    Ok(())
}

fn perform_measurements<R: Rng + ?Sized>(
    graph: &GenericGraph,
    parameters: &Parameters,
    rng: &mut R,
    i: usize,
) -> anyhow::Result<()> {
    let mut temp_filepath = parameters.temp_dir.clone();
    debug!("Temporary directory used is {:?}", &temp_filepath);
    temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    info!(
        "Creating temporary file {} for distance matrix",
        temp_filepath.to_string_lossy()
    );
    let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
        graph,
        parameters.max_distance,
        &dmatrix_file,
    )?;

    let ddist_dataset = dmatrix.dataset(distance_matrix::DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let ddist = ddist_dataset.read_1d::<u64>()?;
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u64>()?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let ddist_dataset = datafile.dataset(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset.write_slice(&ddist, s![i, ..])?;
    let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset.write_slice(&sphere_volumes, s![i, .., ..])?;
    datafile.close()?;

    // debug!("Sampling point to calculate the observables for");
    // let sample_iter = std::iter::repeat_with(|| graph.sample_node(rng));
    // let samples = Array1::from_iter(sample_iter.take(SAMPLE_SIZE).map(usize::from));
    // debug!("Obtained samples: {}", samples.slice(s![0usize..10]));
    // warn!("Writing samples to disk. Do NOT stop the simulation");
    // let datafile = parameters.open_datafile()?;
    // let dataset = datafile.dataset(SAMPLES_DATASET_NAME)?;
    // dataset.write_slice(&samples, s![i, ..])?;
    // datafile.close()?;

    // let max_delta = parameters.max_delta;
    // info!("Measuring {SAMPLE_SIZE} samples of the average sphere profile on the graph");
    // let deltas: Vec<usize> = (1..max_delta + 1).collect();
    // use average_sphere_distance::single_sphere::profile_points_full_graph_from_disk as asd_profile;
    // let asd = asd_profile::<u8, u32, _>(&dmatrix, samples.view(), deltas.iter().copied());
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = parameters.open_datafile()?;
    // let dataset = datafile.dataset(ASD_DATASET_NAME)?;
    // dataset.write_slice(&asd, s![i, .., ..])?;
    // datafile.close()?;

    // info!(
    //     "Measuring full graph of the average sphere profile for deltas: {}",
    //     CORRELATION_DELTAS.clone().collect::<Array1<usize>>()
    // );
    // use average_sphere_distance::single_sphere::profile_full_graph_from_disk as full_asd_profile;
    // let asd = full_asd_profile::<u8, u32, _>(&dmatrix, CORRELATION_DELTAS);
    // warn!("Writing observables to disk. Do NOT stop the simulation");
    // let datafile = parameters.open_datafile()?;
    // let dataset = datafile.dataset(ASD_FULL_DATASET_NAME)?;
    // dataset.write_slice(&asd, s![i, .., ..])?;
    // datafile.close()?;

    info!("Measuring vertex degree distributions on the graph");
    let degree_field = vertex_degree_distribution::field_from_graph(graph);
    let nbr_degree_field = vertex_degree_distribution::average_neighbour_field_from_disk(&dmatrix)?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let degree_dataset = datafile.dataset(DEGREE_DATASET_NAME)?;
    degree_dataset.write_slice(&degree_field, s![i, ..])?;
    let nbr_dataset = datafile.dataset(NBR_DEGREE_DATASET_NAME)?;
    nbr_dataset.write_slice(&nbr_degree_field, s![i, ..])?;
    datafile.close()?;

    // Two point functions
    let max_correlation_distance = parameters.max_correlation_distance;
    let correlation_distances = 1..max_correlation_distance + 1;
    info!("Sampling correlation pairs uniformly");
    let (two_point_samples0, two_point_samples1) =
        two_point_function::sample_uniform_full_graph_from_disk(
            &dmatrix,
            correlation_distances.clone(),
            TWO_POINT_SAMPLE_SIZE,
            rng,
        )?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let samples2p_dataset = datafile.dataset(SAMPLES2P_UNIFORM_DATASET_NAME)?;
    samples2p_dataset.write_slice(&two_point_samples0, s![0, i, .., ..])?;
    samples2p_dataset.write_slice(&two_point_samples1, s![1, i, .., ..])?;
    datafile.close()?;

    info!("Sampling correlation pairs from origin");
    let (two_point_samples0, two_point_samples1) =
        two_point_function::sample_from_origin_full_graph_from_disk(
            &dmatrix,
            correlation_distances,
            TWO_POINT_SAMPLE_SIZE,
            rng,
        )?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let samples2p_dataset = datafile.dataset(SAMPLES2P_ORIGIN_DATASET_NAME)?;
    samples2p_dataset.write_slice(&two_point_samples0, s![0, i, .., ..])?;
    samples2p_dataset.write_slice(&two_point_samples1, s![1, i, .., ..])?;
    datafile.close()?;

    info!("Finished observable measurements");
    let datafile = parameters.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    Ok(())
}
