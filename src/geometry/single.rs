use distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::graph::GenericGraph;
use dyntri::io::graph::import_adjacency_list;
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::{average_sphere_distance, vertex_degree_distribution};

use log::{debug, info, warn};
use ndarray::prelude::*;

const ADJACENCY_LIST_FILENAME: &str = "./data/adjacency_poisson_periodic_plane_154036";
const MEASUREMENT_RESULTS_FILENAME: &str = "./measurement-periodic-plane-150k.h5";

const TEMPORARY_FILEPATH: &str = ".temp_distance_matrix.h5";
const ASD_DATASET_NAME: &str = "average-sphere-distance";
const DEGREE_DATASET_NAME: &str = "vertex-degree-field";
const NBR_DEGREE_DATASET_NAME: &str = "average-neighbour-degree-field";
const DISTANCE_DISTRIBUTION_DATASET_NAME: &str = "distance-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const MAX_DISTANCE: usize = 200; // Should be at least 2*MAX_DELTA

fn load_graph_from_adjacency(file: std::fs::File) -> GenericGraph {
    let nodes = import_adjacency_list(file).expect("Could not load graph from file.");
    GenericGraph::new(nodes)
}

fn create_datafile() -> hdf5::Result<hdf5::File> {
    hdf5::File::create_excl(MEASUREMENT_RESULTS_FILENAME)
}

fn open_datafile() -> hdf5::Result<hdf5::File> {
    hdf5::File::open_rw(MEASUREMENT_RESULTS_FILENAME)
}

pub fn start_geometry_measurement() -> hdf5::Result<()> {
    debug!("Creating datafile for measurement storage");
    create_datafile()?.close()?;
    info!("Loading graph from adjacency list");
    let adj_file =
        std::fs::File::open(ADJACENCY_LIST_FILENAME).expect("Could not open adjacency list");
    let graph = load_graph_from_adjacency(adj_file);
    info!("Performing observable measurements");
    perform_measurements(&graph)
}

fn perform_measurements(graph: &GenericGraph) -> hdf5::Result<()> {
    info!("Creating temporary file {TEMPORARY_FILEPATH} for distance matrix");
    let dmatrix_file = hdf5::File::create(TEMPORARY_FILEPATH)?;
    let dmatrix =
        compute_distance_matrix_with_masks_disk::<u8>(graph, MAX_DISTANCE, &dmatrix_file)?;

    let ddist_dataset = dmatrix.dataset(distance_matrix::DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    let ddist = ddist_dataset.read_1d::<u64>()?;
    // Make estimate of r_eff, i.e. half the effective size in the smallest dimension
    let effective_radius = ddist
        .indexed_iter()
        .max_by(|prev, next| prev.1.cmp(next.1))
        .expect("The distance distribution must not be empty")
        .0;
    info!(
        "Determined effective radius to be {effective_radius}, \
        using this value as maximum distance for correlations"
    );
    let max_correlation_distance = effective_radius;
    let max_delta = effective_radius / 2;
    info!("Using maximum delta: {max_delta}, and maximum correlation distance: {max_correlation_distance}");

    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u64>()?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = open_datafile()?;
    let ddist_dataset = datafile
        .new_dataset_builder()
        .with_data(&ddist)
        .create(DISTANCE_DISTRIBUTION_DATASET_NAME)?;
    ddist_dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..MAX_DISTANCE + 1))
        .create("r")?;
    let sphere_vol_dataset = datafile
        .new_dataset_builder()
        .with_data(&sphere_volumes)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..MAX_DISTANCE + 1))
        .create("r")?;
    datafile.close()?;

    info!("Measuring the average sphere profile on the graph");
    let deltas: Vec<usize> = (1..max_delta + 1).collect();
    let asd = average_sphere_distance::single_sphere::profile_full_graph_from_disk::<u8, u32, _>(
        &dmatrix,
        deltas.iter().copied(),
    );
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = open_datafile()?;
    let dataset = datafile
        .new_dataset_builder()
        .with_data(&asd)
        .create(ASD_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&deltas)
        .create("delta")?;
    datafile.close()?;

    info!("Measuring vertex degree distributions on the graph");
    let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;
    let nbr_degree_field = vertex_degree_distribution::average_neighbour_field_from_disk(&dmatrix)?;
    warn!("Writing observables to disk. Do NOT stop the simulation");
    let datafile = open_datafile()?;
    datafile
        .new_dataset_builder()
        .with_data(&degree_field)
        .create(DEGREE_DATASET_NAME)?;
    datafile
        .new_dataset_builder()
        .with_data(&nbr_degree_field)
        .create(NBR_DEGREE_DATASET_NAME)?;
    datafile.close()?;

    info!("Finished observable measurements");

    Ok(())
}
