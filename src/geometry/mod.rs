use std::path::Path;

pub mod diffusion;
pub mod diffusion_periodic;
pub mod diffusion_single;
pub mod double_sphere;
pub mod sampled;
pub mod single;
pub mod two_point_sampled;

#[derive(Debug, Clone, Copy, clap::ValueEnum)]
pub enum Configuration {
    Single,
    Sampled,
    Diffusion,
    DiffusionPeriodic,
    DiffusionSingle,
    DoubleSphere,
    TwoPointSampled,
}

pub fn perform_geometry_measurement(
    config_file: &Path,
    dir_in: &Path,
    configuration: Configuration,
    temp_dir: &Path,
) {
    match configuration {
        Configuration::Single => {
            single::start_geometry_measurement().expect("Something went wrong with HDF5")
        }
        Configuration::Sampled => sampled::start_measurement(config_file, dir_in, temp_dir)
            .expect("Something went wrong with HDF5"),
        Configuration::Diffusion => diffusion::start_measurement(config_file, dir_in, temp_dir)
            .expect("Something went wrong with HDF5"),
        Configuration::DiffusionPeriodic => {
            diffusion_periodic::start_measurement(config_file, dir_in, temp_dir)
                .expect("Something went wrong with HDF5")
        }
        Configuration::DiffusionSingle => diffusion_single::start_measurement(config_file, dir_in)
            .expect("Something went wrong with HDF5"),
        Configuration::DoubleSphere => {
            double_sphere::start_measurement(config_file, dir_in, temp_dir)
                .expect("Something went wrong with HDF5")
        }
        Configuration::TwoPointSampled => {
            two_point_sampled::start_measurement(config_file, dir_in, temp_dir)
                .expect("Something went wrong with HDF5")
        }
    }
}
