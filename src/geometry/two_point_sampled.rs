use std::fs;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use anyhow::Context;
use dyntri::collections::Label;
use dyntri::graph::field::Field;
use dyntri::graph::FieldGraph;
use dyntri::graph::GenericGraph;
use dyntri::io::graph::import_adjacency_list_thijs_patch;
use dyntri::observables::average_sphere_distance::single_sphere;
use dyntri::observables::full_graph::distance_matrix;
use dyntri::observables::full_graph::distance_matrix::compute_distance_matrix_with_masks_disk;
use dyntri::observables::{two_point_function, vertex_degree_distribution};
use hdf5::types::VarLenUnicode;
use log::{debug, info, trace, warn};
use ndarray::prelude::*;
use rand::{Rng, SeedableRng};

const CURRENT_MEASUREMENT_ATTR_NAME: &str = "current-measurement";
const SIZE_ATTR_NAME: &str = "graph-size";
const CONFIGURATIONS_ATTR_NAME: &str = "configurations";

const DISTANCE_MATRIX_FILENAME: &str = ".temp_distance_matrix.h5";
const DEGREE_DISTRIBUTION_DATASET_NAME: &str = "vertex-degree-distribution";
const SPHERE_VOLUMES_DATASET_NAME: &str = "sphere-volumes";
const TWO_POINT_DEGREE_DATASET_NAME: &str = "two-point-vertex-degree";
const TWO_POINT_ASD_DATASET_NAME: &str = "two-point-average-sphere-distance";
const TWO_POINT_SVOL_DATASET_NAME: &str = "two-point-sphere-volume";

/// A maximum vertex degree of 80 is very conservative, the highest is likely a lot lower
const MAX_VERTEX_DEGREE: usize = 80;
const TWO_POINT_SAMPLE_SIZE: usize = 500;

const EFFECTIVE_RADIUS_FACTOR: f32 = 1.56;
const EFFECTIVE_DIMENSION: f32 = 4.0;
const MAX_DISTANCE_FACTOR: f32 = 1.3;
const FIXED_MAX_DELTA: usize = 12;

fn create_hdf5_string_array<S: AsRef<str>>(
    list: &[S],
) -> Result<Array1<VarLenUnicode>, <VarLenUnicode as FromStr>::Err> {
    list.iter()
        .map(|s| VarLenUnicode::from_str(s.as_ref()))
        .collect()
}

struct Parameters {
    size: usize,
    configurations: usize,
    max_distance: usize,
    max_delta: usize,
    max_correlation_distance: usize,
    temp_dir: PathBuf,
    file_out: PathBuf,
}

impl Parameters {
    fn open_datafile(&self) -> hdf5::Result<hdf5::File> {
        hdf5::File::open_rw(&self.file_out)
    }
}

pub fn start_measurement(file_out: &Path, dir_in: &Path, temp_dir: &Path) -> anyhow::Result<()> {
    info!("Loading files from directory {:?}", dir_in);
    let adj_filenames: Vec<PathBuf> = fs::read_dir(dir_in)?
        .map(|entry| entry.map(|e| e.path()))
        .collect::<Result<_, _>>()?;

    let size = get_size(
        adj_filenames
            .first()
            .with_context(|| "No files in directory")?,
    )?;
    let effective_radius = EFFECTIVE_RADIUS_FACTOR * (size as f32).powf(1.0 / EFFECTIVE_DIMENSION);
    let max_distance = (MAX_DISTANCE_FACTOR * effective_radius) as usize;
    info!("Using maximum distance of {max_distance}");
    let parameters = Parameters {
        size,
        configurations: adj_filenames.len(),
        max_distance,
        max_delta: (max_distance / 3).min(FIXED_MAX_DELTA),
        max_correlation_distance: max_distance,
        temp_dir: temp_dir.to_path_buf(),
        file_out: file_out.to_path_buf(),
    };

    debug!("Creating datafile for measurement storage");
    let file = hdf5::File::create_excl(file_out)?;
    info!("Initializing attributes in output file");
    initialize_attributes(&file, &parameters)?;
    info!("Initializing observales in output file");
    initialize_observables(&file, &parameters)?;
    file.close()?;

    info!("Starting observable measurements");
    start_measurements(&adj_filenames, &parameters)
}

fn get_size(filename: &Path) -> anyhow::Result<usize> {
    let adj_file = fs::File::open(filename).with_context(|| "Could not open file")?;
    Ok(BufReader::new(adj_file).lines().count())
}

fn start_measurements(files: &[PathBuf], parameters: &Parameters) -> anyhow::Result<()> {
    let mut rng = rand_xoshiro::Xoshiro256PlusPlus::from_entropy();
    for (i, filename) in files.iter().enumerate() {
        info!(
            "Starting measurement {}/{}",
            i + 1,
            parameters.configurations
        );
        debug!("Loading graph from file {:?}", filename);
        let adj_file = std::fs::File::open(filename)?;
        let graph = load_graph_from_adjacency(adj_file)
            .with_context(|| "Could not load graph from file.")?;
        perform_measurements(&graph, parameters, &mut rng, i)?;
    }
    Ok(())
}

fn load_graph_from_adjacency(file: std::fs::File) -> anyhow::Result<GenericGraph> {
    // let nodes = import_adjacency_list(file)?;
    let nodes = import_adjacency_list_thijs_patch(file)?;
    Ok(GenericGraph::new(nodes))
}

fn initialize_attributes(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    file.new_attr_builder()
        .with_data(arr0(0).view())
        .create(CURRENT_MEASUREMENT_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.size).view())
        .create(SIZE_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.configurations).view())
        .create(CONFIGURATIONS_ATTR_NAME)?;

    Ok(())
}

/// Initialize the dataset for each observable
fn initialize_observables(file: &hdf5::File, parameters: &Parameters) -> hdf5::Result<()> {
    debug!("Preparing vertex degree distribution dataset");
    let dataset = file
        .new_dataset::<u32>()
        .shape([parameters.configurations, MAX_VERTEX_DEGREE])
        .blosc_lz4(9, true)
        .create(DEGREE_DISTRIBUTION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(Array1::from_iter(0..MAX_VERTEX_DEGREE).view())
        .create("degree")?;

    debug!("Preparing average sphere volume dataset");
    let dataset = file
        .new_dataset::<f32>()
        .shape([2, parameters.configurations, parameters.max_distance + 1])
        .blosc_lz4(9, true)
        .create(SPHERE_VOLUMES_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(
            create_hdf5_string_array(&["statistic", "triangulation", "distance"])
                .expect("In code constructed string should always convert")
                .view(),
        )
        .create("axes")?;
    dataset
        .new_attr_builder()
        .with_data::<'_, _, VarLenUnicode, _>(
            create_hdf5_string_array(&["mean", "std"])
                .expect("In code constructed string should always convert")
                .view(),
        )
        .create("statistic")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..parameters.max_distance + 1))
        .create("radius")?;

    debug!("Preparing two-point sphere volume dataset");
    let dataset = file
        .new_dataset::<u16>()
        .shape([
            2,
            parameters.configurations,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
            parameters.max_distance + 1,
        ])
        .chunk([
            1,
            1,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
            parameters.max_distance + 1,
        ])
        .blosc_lz4(9, true)
        .create(TWO_POINT_SVOL_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(
            create_hdf5_string_array(&[
                "two-point",
                "triangulation",
                "sample",
                "distance",
                "radius",
            ])
            .expect("In code constructed strings should always convert")
            .view(),
        )
        .create("axes")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            0..parameters.max_correlation_distance + 1,
        ))
        .create("distance")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(0..parameters.max_distance + 1))
        .create("radius")?;

    debug!("Preparing two-point vertex degree dataset");
    let dataset = file
        .new_dataset::<u8>()
        .shape([
            2,
            parameters.configurations,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
        ])
        .chunk([
            1,
            1,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
        ])
        .blosc_lz4(9, true)
        .create(TWO_POINT_DEGREE_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(
            create_hdf5_string_array(&["two-point", "triangulation", "sample", "distance"])
                .expect("In code constructed string should always convert")
                .view(),
        )
        .create("axes")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            0..parameters.max_correlation_distance + 1,
        ))
        .create("distance")?;

    debug!("Preparing two-point average sphere distance dataset");
    let dataset = file
        .new_dataset::<f64>()
        .shape([
            2,
            parameters.configurations,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
            parameters.max_delta,
        ])
        .chunk([
            1,
            1,
            TWO_POINT_SAMPLE_SIZE,
            parameters.max_correlation_distance + 1,
            parameters.max_delta,
        ])
        .blosc_lz4(9, true)
        .create(TWO_POINT_ASD_DATASET_NAME)?;
    trace!("Adding attributes to two-point average sphere distance dataset");
    dataset
        .new_attr_builder()
        .with_data(
            create_hdf5_string_array(&[
                "two-point",
                "triangulation",
                "sample",
                "distance",
                "delta",
            ])
            .expect("In code constructed strings should always convert")
            .view(),
        )
        .create("axes")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(
            0..parameters.max_correlation_distance + 1,
        ))
        .create("distance")?;
    dataset
        .new_attr_builder()
        .with_data(&Array1::from_iter(1..parameters.max_delta + 1))
        .create("delta")?;

    debug!("Finished setting up the HDF5 files");
    Ok(())
}

fn perform_measurements<R: Rng + ?Sized>(
    graph: &GenericGraph,
    parameters: &Parameters,
    rng: &mut R,
    i: usize,
) -> hdf5::Result<()> {
    info!("Performing observable measurements");

    // Computing distance matrix
    let mut temp_filepath = parameters.temp_dir.clone();
    debug!("Temporary directory used is {:?}", &temp_filepath);
    temp_filepath.push(DISTANCE_MATRIX_FILENAME);
    info!(
        "Creating temporary file {} for distance matrix",
        temp_filepath.to_string_lossy()
    );
    let dmatrix_file = hdf5::File::create(&temp_filepath)?;
    let dmatrix = compute_distance_matrix_with_masks_disk::<u8>(
        graph,
        parameters.max_distance,
        &dmatrix_file,
    )?;

    // Average sphere volume
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<f32>()?;
    let svol_mean = sphere_volumes
        .mean_axis(Axis(1))
        .expect("Axis 1 should not have vertex_count length not 0");
    let svol_std = sphere_volumes.std_axis(Axis(1), 1.0);

    warn!("Writing sphere volumes to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let sphere_vol_dataset = datafile.dataset(SPHERE_VOLUMES_DATASET_NAME)?;
    sphere_vol_dataset.write_slice(&svol_mean, s![0, i, ..])?;
    sphere_vol_dataset.write_slice(&svol_std, s![1, i, ..])?;
    datafile.close()?;

    // Two point samples
    let max_correlation_distance = parameters.max_correlation_distance;
    let correlation_distances = 0..max_correlation_distance + 1;
    info!(
        "Sampling {} correlation pairs uniformly up to distance {}",
        TWO_POINT_SAMPLE_SIZE, max_correlation_distance
    );
    let (two_point_samples0, two_point_samples1) =
        two_point_function::sample_uniform_full_graph_from_disk(
            &dmatrix,
            correlation_distances,
            TWO_POINT_SAMPLE_SIZE,
            rng,
        )?;
    let two_point_samples0 = two_point_samples0.reversed_axes();
    let two_point_samples1 = two_point_samples1.reversed_axes();

    // Track the index where the sample will be stored
    let mut sample_index = graph.new_field();
    let mut index: usize = 0;
    // List of the sample labels where each index in `sample_index` point to the sample in this list
    let samples: Array1<usize> = two_point_samples0
        .iter()
        .chain(two_point_samples1.iter())
        .copied()
        .filter(|&point| {
            let unset = sample_index.set(Label::from(point), index);
            if unset {
                index += 1;
            }
            unset
        })
        .collect();

    // Sphere volumes two-point function
    debug!("Determining sphere volume two point functions on the graph");
    let sphere_vol_dataset = dmatrix.dataset(distance_matrix::SPHERE_VOLUMES_DATASET_NAME)?;
    let sphere_volumes = sphere_vol_dataset.read_2d::<u16>()?;
    let svol_sample0 = two_point_samples0
        .iter()
        .flat_map(|&ilabel| sphere_volumes.column(ilabel).into_iter().copied())
        .collect::<Array1<u16>>()
        .into_shape([
            two_point_samples0.shape()[0],
            two_point_samples0.shape()[1],
            sphere_volumes.shape()[0],
        ])
        .expect("Shape should be correct by construction");
    let svol_sample1 = two_point_samples1
        .iter()
        .flat_map(|&ilabel| sphere_volumes.column(ilabel).into_iter().copied())
        .collect::<Array1<u16>>()
        .into_shape([
            two_point_samples1.shape()[0],
            two_point_samples1.shape()[1],
            sphere_volumes.shape()[0],
        ])
        .expect("Shape should be correct by construction");
    warn!("Writing sphere volume two point functions to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let twopoint_svol_dataset = datafile.dataset(TWO_POINT_SVOL_DATASET_NAME)?;
    twopoint_svol_dataset.write_slice(&svol_sample0, s![0, i, .., .., ..])?;
    twopoint_svol_dataset.write_slice(&svol_sample1, s![1, i, .., .., ..])?;
    datafile.close()?;

    // Measure vertex degrees
    info!("Measuring vertex degree field on the graph");
    let degree_field = vertex_degree_distribution::field_from_disk(&dmatrix)?;

    let mut degree_distribution: Array1<u32> = Array1::zeros([MAX_VERTEX_DEGREE]);
    for &degree in degree_field.iter() {
        if degree >= degree_distribution.len() {
            warn!(
                "Measured vertex degree {}; higher than maximum expected degree",
                degree
            );
        }
        degree_distribution[degree] += 1;
    }
    warn!("Writing degree distribution to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let degree_dataset = datafile.dataset(DEGREE_DISTRIBUTION_DATASET_NAME)?;
    degree_dataset.write_slice(&degree_distribution, s![i, ..])?;
    datafile.close()?;

    debug!("Determining vertex degree two point functions on the graph");
    let degree_sample0: Array2<u8> = two_point_samples0.mapv(|ilabel| degree_field[ilabel] as u8);
    let degree_sample1: Array2<u8> = two_point_samples1.mapv(|ilabel| degree_field[ilabel] as u8);

    warn!("Writing vertex degree two point function to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let twopoint_degree_dataset = datafile.dataset(TWO_POINT_DEGREE_DATASET_NAME)?;
    twopoint_degree_dataset.write_slice(&degree_sample0.as_standard_layout(), s![0, i, .., ..])?;
    twopoint_degree_dataset.write_slice(&degree_sample1.as_standard_layout(), s![1, i, .., ..])?;
    datafile.close()?;

    let max_delta = parameters.max_delta;
    let deltas = 1..max_delta + 1;
    let asd_profile = single_sphere::profile_points_full_graph_from_disk::<u16, u32, _>(
        &dmatrix,
        samples.view(),
        deltas,
    );
    let asd_sample0 = two_point_samples0
        .iter()
        .flat_map(|&ilabel| {
            let index = sample_index
                .get(Label::from(ilabel))
                .expect("label must exist by construction");
            asd_profile.column(index).into_iter().copied()
        })
        .collect::<Array1<f64>>()
        .into_shape([
            two_point_samples0.shape()[0],
            two_point_samples0.shape()[1],
            asd_profile.shape()[0],
        ])
        .expect("Shape should be correct by construction");
    let asd_sample1 = two_point_samples1
        .iter()
        .flat_map(|&ilabel| {
            let index = sample_index
                .get(Label::from(ilabel))
                .expect("label must exist by construction");
            asd_profile.column(index).into_iter().copied()
        })
        .collect::<Array1<f64>>()
        .into_shape([
            two_point_samples1.shape()[0],
            two_point_samples1.shape()[1],
            asd_profile.shape()[0],
        ])
        .expect("Shape should be correct by construction");

    warn!("Writing ASD two point functions to disk. Do NOT stop the simulation");
    let datafile = parameters.open_datafile()?;
    let twopoint_asd_dataset = datafile.dataset(TWO_POINT_ASD_DATASET_NAME)?;
    twopoint_asd_dataset.write_slice(&asd_sample0, s![0, i, .., .., ..])?;
    twopoint_asd_dataset.write_slice(&asd_sample1, s![1, i, .., .., ..])?;
    datafile.close()?;

    // Clean up
    info!("Finished observable measurements");
    let datafile = parameters.open_datafile()?;
    let current_measurement = datafile.attr(CURRENT_MEASUREMENT_ATTR_NAME)?;
    current_measurement.write_scalar(&(i + 1))?;

    dmatrix_file.close()?;

    Ok(())
}
