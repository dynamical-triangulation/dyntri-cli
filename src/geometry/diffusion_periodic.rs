use std::fs::File;
use std::path::{Path, PathBuf};

use dyntri::graph::periodic2d_generic_graph::bounded_graph::BoundedPeriodic2DGenericGraph;
use dyntri::graph::periodic2d_generic_graph::Periodic2DGenericGraph;
use dyntri::graph::periodic_graph::PeriodicGraph;
use dyntri::graph::{Graph, SampleGraph};
use dyntri::io::graph::{import_adjacency_list, import_boundary2d_adjacency_list};
use dyntri::observables::diffusion;
use log::{debug, info, warn};
use ndarray::prelude::*;
use rand::{Rng, SeedableRng};
use rand_xoshiro::Xoshiro256PlusPlus;

const DIFFUSION_DATASET_NAME: &str = "return-probability-periodic";

const CURRENT_MEASUREMENT_ATTR_NAME: &str = "current-measurement";
const SIZE_ATTR_NAME: &str = "graph-size";

const DIFFUSION_SAMPLE_SIZE: usize = 5;
const DIFFUSION_COEFFICIENT: f64 = 0.90;

const MAX_X_PATCHES: u8 = 2;
const MAX_Y_PATCHES: u8 = 2;
const MAX_DIFFUSION_TIME: usize = 30_000;
// const EFFECTIVE_RADIUS_FACTOR: f32 = 1.56;
// const EFFECTIVE_DIMENSION: f32 = 4.0;
// const MAX_DISTANCE_FACTOR: f32 = 1.75;
// const MAX_DIFFUSION_TIME_FACTOR: f32 = 0.17;

struct Parameters {
    size: usize,
    // max_distance: usize,
    max_diffusion_time: usize,
    file_out: PathBuf,
}

impl Parameters {
    fn open_datafile(&self) -> hdf5::Result<hdf5::File> {
        hdf5::File::open_rw(&self.file_out)
    }
}

#[allow(unused_variables)]
pub fn start_measurement(file_out: &Path, adj_in: &Path, bound_in: &Path) -> anyhow::Result<()> {
    let adj_file = File::open(adj_in)?;
    let adj_graph = import_adjacency_list(adj_file)?;
    let bnd_file = File::open(bound_in)?;
    let bounds = import_boundary2d_adjacency_list(bnd_file)?;
    let graph = Periodic2DGenericGraph::from_nodes_and_boundary_vec(adj_graph, bounds)?;
    let bounded_graph =
        BoundedPeriodic2DGenericGraph::from_periodic_graph(graph, MAX_X_PATCHES, MAX_Y_PATCHES);

    let size = bounded_graph.base_len();

    // let effective_radius = EFFECTIVE_RADIUS_FACTOR * (size as f32).powf(1.0 / EFFECTIVE_DIMENSION);
    // let max_distance = (MAX_DISTANCE_FACTOR * effective_radius) as usize;
    // let max_diffusion_time = (MAX_DIFFUSION_TIME_FACTOR * size as f32) as usize;
    let max_diffusion_time = MAX_DIFFUSION_TIME;
    info!("Using graph of size {size} and max diffusion time of {max_diffusion_time}");
    let parameters = Parameters {
        size,
        // max_distance,
        max_diffusion_time,
        file_out: file_out.to_path_buf(),
    };

    debug!("Creating datafile for measurement storage");
    let file = hdf5::File::create_excl(file_out)?;
    info!("Initializing attributes in output file");
    initialize_attributes(&file, &parameters)?;
    info!("Initializing observables in output file");
    initialize_observables(&file, &parameters)?;
    file.close()?;

    info!("Starting observable measurements");
    let mut rng = Xoshiro256PlusPlus::from_entropy();
    perform_measurements(&bounded_graph, &parameters, &mut rng)
}

fn initialize_attributes(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    file.new_attr_builder()
        .with_data(arr0(0).view())
        .create(CURRENT_MEASUREMENT_ATTR_NAME)?;

    file.new_attr_builder()
        .with_data(arr0(parameters.size).view())
        .create(SIZE_ATTR_NAME)?;

    Ok(())
}

/// Initialize the dataset for each observable
fn initialize_observables(file: &hdf5::File, parameters: &Parameters) -> anyhow::Result<()> {
    debug!("Preparing diffusion dataset");
    let dataset = file
        .new_dataset::<f32>()
        .shape([DIFFUSION_SAMPLE_SIZE, parameters.max_diffusion_time + 1])
        .chunk([1, parameters.max_diffusion_time + 1])
        .blosc_lz4(9, true)
        .create(DIFFUSION_DATASET_NAME)?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(0))
        .create("sigma-start")?;
    dataset
        .new_attr_builder()
        .with_data(&arr0(parameters.max_diffusion_time))
        .create("sigma-max")?;

    Ok(())
}

fn perform_measurements<R: Rng + ?Sized>(
    graph: &BoundedPeriodic2DGenericGraph,
    parameters: &Parameters,
    rng: &mut R,
) -> anyhow::Result<()> {
    info!("Measuring vertex degree distributions on the graph");

    // Sample a set of points in the graph to use for diffusion measurements
    let points: Vec<<Periodic2DGenericGraph as Graph>::Label> = (0..DIFFUSION_SAMPLE_SIZE)
        .map(|_| graph.sample_node(rng))
        .collect();

    let max_diffusion_time = parameters.max_diffusion_time;
    debug!("Measuring return probability on graph");
    for (j, &point) in points.iter().enumerate() {
        let rprop = Array1::from_vec(
            diffusion::measure_bounded(graph, point, max_diffusion_time + 1, DIFFUSION_COEFFICIENT)
                .0,
        );
        warn!("Writing observables to disk. Do NOT stop the simulation");
        let datafile = parameters.open_datafile()?;
        let return_prob_dataset = datafile.dataset(DIFFUSION_DATASET_NAME)?;
        return_prob_dataset.write_slice(rprop.view(), s![j, ..])?;
        datafile.close()?;
    }

    info!("Finished observable measurements");
    Ok(())
}
