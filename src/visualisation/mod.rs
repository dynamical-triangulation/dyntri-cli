use clap::ValueEnum;

use dyntri::graph::periodic_spacetime_graph::PeriodicSpacetimeGraph;
use dyntri::graph::spacetime_graph::SpacetimeGraph;
use dyntri::graph::{GenericGraph, SizedGraph};
use dyntri::observables::{average_sphere_distance, volume_profile};
use dyntri::triangulation::FullTriangulation;
use dyntri::{collections::Label, monte_carlo::fixed_universe::Universe};
use log::{debug, error, warn};
use rand::SeedableRng;
use rand_xoshiro::Xoshiro256PlusPlus;
use std::{fs::File, io::BufWriter, path::Path};

mod cylinder;
mod flat;
mod torus;

const MARKOV_CHAIN_SWEEPS: f32 = 500.0;
const ASD_FIELD_DELTA: usize = 5;

#[derive(Debug, Clone, Copy, ValueEnum)]
pub enum Embedding {
    FlatSimple,
    FlatHarmonic,
    FlatSliced,
    CylinderSimple,
    CylinderSliced,
    CylinderWarped,
    TorusSimple,
    TorusWarped,
}

pub fn create_visualisation(visualisation_type: Embedding, output_filename: &Path) {
    let mut rng = Xoshiro256PlusPlus::seed_from_u64(42);
    // let mut universe = Universe::default(8_000, 97);
    // let mut universe = Universe::default(2592, 27);
    let mut universe = Universe::default(450, 15);
    // let mut universe = Universe::default(2400, 15);

    let size = universe.size();
    // let steps = (0.0 * size as f32) as usize;
    let steps = (MARKOV_CHAIN_SWEEPS * size as f32) as usize;
    for _ in 0..steps {
        universe.step(&mut rng);
    }
    debug!(
        "Created a universe with {} triangles and {} timeslices",
        universe.size(),
        universe.timeslices()
    );
    // let universe = dynamic_universe::Universe::from_fixed_universe(&universe);

    universe.triangulation.flip(Label::from(14));
    universe.triangulation.flip(Label::from(408));
    // universe
    //     .triangulation
    //     .relocate(Label::from(17), Label::from(12));
    // universe.triangulation.remove(Label::from(25));

    // universe.triangulation.flip(Label::from(14));
    // universe.triangulation.flip(Label::from(18));
    // universe.triangulation.flip(Label::from(9));
    // universe.triangulation.remove(Label::from(14));
    // universe.triangulation.flip(Label::from(18));
    // universe.triangulation.flip(Label::from(9));

    match visualisation_type {
        Embedding::FlatHarmonic | Embedding::FlatSimple | Embedding::FlatSliced => {
            export_flat_embedding(visualisation_type, output_filename, &universe)
        }
        Embedding::CylinderSimple | Embedding::CylinderSliced | Embedding::CylinderWarped => {
            export_cylinder_embedding(visualisation_type, output_filename, &universe)
        }
        Embedding::TorusSimple | Embedding::TorusWarped => {
            export_torus_embedding(visualisation_type, output_filename, &universe)
        }
    };
}

fn export_flat_embedding(
    visualisation_type: Embedding,
    output_filename: &Path,
    // universe: &dynamic_universe::Universe,
    universe: &Universe,
) {
    let (triangulation, mapping) =
        FullTriangulation::from_triangulation_with_mapping(&universe.triangulation);
    let graph = PeriodicSpacetimeGraph::from_triangulation_vertex(&triangulation);

    let embedding = match visualisation_type {
        Embedding::FlatHarmonic => flat::create_harmonic_embedding(&triangulation, &graph),
        Embedding::FlatSliced => flat::create_sliced_embedding(&triangulation, &graph),
        Embedding::FlatSimple => flat::create_simple_embedding(&triangulation, &graph),
        _ => unreachable!(),
    };

    let file = match File::create(output_filename) {
        Ok(file) => file,
        Err(e) => {
            error!("Could not create visualisation file: {e}");
            return;
        }
    };
    let mut buffer = BufWriter::new(file);
    // let sizedgraph = GenericGraph::from_spacetime_graph(&SpacetimeGraph::from_triangulation_dual(
    //     &triangulation,
    // ));
    // let mut diff = diffusion::iterable_sized::<_, generic_graph::BaseIterator, _>(
    //     &sizedgraph,
    //     Label::from(0),
    //     1.0,
    // );
    // for _ in 0..1 + 1 {
    //     diff.next();
    // }
    // let pfield = diff.current_state();
    // let parray = Array1::from_iter(
    //     sizedgraph
    //         .iter()
    //         .map(|label| pfield.get(label).as_ref().copied().unwrap_or_else(|| 0.0)),
    // );
    let sizedgraph = GenericGraph::from_spacetime_graph(
        &SpacetimeGraph::from_triangulation_vertex(&triangulation),
    );
    println!("length {}", sizedgraph.len());
    // let asd = average_sphere_distance::single_sphere::measure_full_graph::<u8, u32>(
    //     &sizedgraph,
    //     ASD_FIELD_DELTA,
    // );
    let mut exported_embedding = embedding.export(9);
    exported_embedding
        .triangles
        .iter_mut()
        .for_each(|triangle| triangle.label = mapping[Label::from(triangle.label)].into());
    // let mut exported_embedding = embedding.export_with_field(9, asd.view());
    // let mut exported_embedding = embedding.export_with_dual_field(9, parray.view());
    // exported_embedding
    //     .embedding
    //     .triangles
    //     .iter_mut()
    //     .for_each(|triangle| triangle.label = mapping[Label::from(triangle.label)].into());
    if let Err(e) = exported_embedding.write(&mut buffer) {
        warn!("Visualisation file could not be written to correctly: {e}");
    }
}

fn export_cylinder_embedding(
    visualisation_type: Embedding,
    output_filename: &Path,
    // universe: &dynamic_universe::Universe,
    universe: &Universe,
) {
    let (mut triangulation, mapping) =
        FullTriangulation::from_triangulation_with_mapping(&universe.triangulation);
    let tmin = volume_profile::measure(&triangulation)
        .volumes()
        .iter()
        .enumerate()
        .min_by_key(|v| v.1)
        .unwrap()
        .0;
    triangulation.time_shift(triangulation.time_len() - tmin);
    // triangulation.time_shift(triangulation.time_len() - 1);

    let embedding = match visualisation_type {
        Embedding::CylinderSimple => cylinder::create_simple_embedding(&triangulation),
        Embedding::CylinderSliced => cylinder::create_sliced_embedding(&triangulation),
        Embedding::CylinderWarped => cylinder::create_constant_edges_embedding(&triangulation),
        _ => unreachable!(),
    };

    let file = match File::create(output_filename) {
        Ok(file) => file,
        Err(e) => {
            error!("Could not create visualisation file: {e}");
            return;
        }
    };
    let mut buffer = BufWriter::new(file);

    let sizedgraph = GenericGraph::from_spacetime_graph(
        &SpacetimeGraph::from_triangulation_vertex(&triangulation),
    );
    eprintln!("size: {}", sizedgraph.len());
    let asd = average_sphere_distance::single_sphere::measure_full_graph::<u8, u32>(
        &sizedgraph,
        ASD_FIELD_DELTA,
    );
    eprintln!("size asd: {}", asd.len());
    let mut exported_embedding = embedding.export_with_field(asd.view());
    debug!(
        "Exporting {} triangles",
        exported_embedding.embedding.triangles.len()
    );

    exported_embedding
        .embedding
        .triangles
        .iter_mut()
        .for_each(|triangle| triangle.label = mapping[Label::from(triangle.label)].into());
    if let Err(e) = exported_embedding.write(&mut buffer) {
        warn!("Visualisation file could not be written to correctly: {e}");
    }
}

fn export_torus_embedding(
    visualisation_type: Embedding,
    output_filename: &Path,
    // universe: &dynamic_universe::Universe,
    universe: &Universe,
) {
    let (mut triangulation, mapping) =
        FullTriangulation::from_triangulation_with_mapping(&universe.triangulation);
    let tmin = volume_profile::measure(&triangulation)
        .volumes()
        .iter()
        .enumerate()
        .min_by_key(|v| v.1)
        .unwrap()
        .0;
    triangulation.time_shift(triangulation.time_len() - tmin);

    let embedding = match visualisation_type {
        Embedding::TorusSimple => torus::create_simple_embedding(&triangulation),
        Embedding::TorusWarped => torus::create_minimal_embedding(&triangulation),
        _ => unreachable!(),
    };

    let sizedgraph = GenericGraph::from_spacetime_graph(
        &SpacetimeGraph::from_triangulation_vertex(&triangulation),
    );
    eprintln!("size: {}", sizedgraph.len());
    let asd = average_sphere_distance::single_sphere::measure_full_graph::<u8, u32>(
        &sizedgraph,
        ASD_FIELD_DELTA,
    );
    eprintln!("size asd: {}", asd.len());
    let mut exported_embedding = embedding.export_with_field(asd.view());
    debug!(
        "Exporting {} triangles",
        exported_embedding.embedding.triangles.len()
    );
    exported_embedding
        .embedding
        .triangles
        .iter_mut()
        .for_each(|triangle| triangle.label = mapping[Label::from(triangle.label)].into());

    let file = match File::create(output_filename) {
        Ok(file) => file,
        Err(e) => {
            error!("Could not create visualisation file: {e}");
            return;
        }
    };
    let mut buffer = BufWriter::new(file);
    if let Err(e) = exported_embedding.write(&mut buffer) {
        warn!("Visualisation file could not be written to correctly: {e}");
    }
}
