use dyntri::graph::periodic_spacetime_graph::PeriodicSpacetimeGraph;
use dyntri::triangulation::FullTriangulation;
use dyntri::visualisation::periodic_embedding2d::PeriodicEmbedding2D;

pub fn create_simple_embedding<'a, 'b>(
    triangulation: &'a FullTriangulation,
    graph: &'b PeriodicSpacetimeGraph,
) -> PeriodicEmbedding2D<'a, 'b> {
    PeriodicEmbedding2D::initialize(triangulation, graph)
}

pub fn create_sliced_embedding<'a, 'b>(
    triangulation: &'a FullTriangulation,
    graph: &'b PeriodicSpacetimeGraph,
) -> PeriodicEmbedding2D<'a, 'b> {
    let mut embedding = create_simple_embedding(triangulation, graph);
    embedding.shift_slices();

    embedding
}

pub fn create_harmonic_embedding<'a, 'b>(
    triangulation: &'a FullTriangulation,
    graph: &'b PeriodicSpacetimeGraph,
) -> PeriodicEmbedding2D<'a, 'b> {
    let mut embedding = create_sliced_embedding(triangulation, graph);
    embedding.make_harmonic();

    embedding
}
