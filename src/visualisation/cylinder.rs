use dyntri::triangulation::FullTriangulation;
use dyntri::visualisation::cylinder_embedding::CylindricalEmbedding;

pub fn create_simple_embedding(triangulation: &FullTriangulation) -> CylindricalEmbedding {
    CylindricalEmbedding::initialize(triangulation)
}

pub fn create_sliced_embedding(triangulation: &FullTriangulation) -> CylindricalEmbedding {
    let mut embedding = create_simple_embedding(triangulation);
    embedding.rotate_slices();
    embedding
}

pub fn create_constant_edges_embedding(triangulation: &FullTriangulation) -> CylindricalEmbedding {
    let mut embedding = create_sliced_embedding(triangulation);
    // embedding.minimize_edge_lengths_inflate(0.5, 0.3e-5);
    embedding.minimize_edge_lengths_inflate(0.5, 0.6e-5);
    embedding
}
