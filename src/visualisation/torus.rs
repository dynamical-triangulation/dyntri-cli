use dyntri::{triangulation::FullTriangulation, visualisation::torus_embedding::TorusEmbedding};

pub fn create_simple_embedding(triangulation: &FullTriangulation) -> TorusEmbedding {
    TorusEmbedding::initialize(triangulation)
}

pub fn create_minimal_embedding(triangulation: &FullTriangulation) -> TorusEmbedding {
    let mut embedding = create_simple_embedding(triangulation);
    embedding.minimize_edge_lengths();
    embedding
}
