use anyhow::{Context, Result};
use log::{debug, error, info, warn};
use std::any::type_name;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::cli::ConfigTask;

mod range;
mod unpack;

const DEFAULT_CONFIG_FILENAME: &str = "config";
const DEFAULT_TOML_EXTENSION: &str = "toml";

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    pub size: usize,
    pub timeslices: usize,
    pub measurements: usize,
    pub burnin: usize,
    pub interval: f32,
    pub output_filepath: Option<PathBuf>,
}

pub fn perform_config_task(task: ConfigTask) {
    use ConfigTask::Unpack;
    match task {
        Unpack {
            file,
            output_dir,
            output_filename,
        } => {
            let result = unpack_config_range(file, output_dir, output_filename);
            if let Err(e) = result {
                error!("Could not unpack the config file: {e:#}");
            }
        }
        _ => unimplemented!(),
    }
}

/// Deserialize TOML file to given type C
pub fn read_toml<'de, C: Deserialize<'de>>(filepath: impl AsRef<Path>) -> Result<C> {
    let mut file = File::open(&filepath).context("Opening TOML file")?;
    let mut toml_str = String::new();
    file.read_to_string(&mut toml_str)
        .context("Reading file to String")?;

    let tomld = toml::Deserializer::new(&toml_str);
    let config: C = serde_ignored::deserialize(tomld, |ignored_path| {
        warn!(
            "Ignoring key \"{ignored_path}\" in {}",
            filepath.as_ref().to_string_lossy()
        )
    })
    .with_context(|| format!("Deserializing TOML to {}", type_name::<C>()))?;
    Ok(config)
}

fn unpack_config_range(
    file: PathBuf,
    output_dir: PathBuf,
    output_filename: Option<String>,
) -> Result<()> {
    info!("Unpacking config file {}", file.to_string_lossy());

    debug!("Parsing config file");
    let config_range: unpack::RangeConfig =
        read_toml(&file).context("Parsing extended config file")?;

    debug!("Unpacking config file");
    let configs = config_range
        .unzip()
        .context("Unpacking extended config file")?;

    let config_amount = configs.len();
    let pad_size = (config_amount.ilog10() + 1) as usize;
    let base_filename = output_filename.unwrap_or_else(|| {
        file.file_stem()
            .and_then(|name| name.to_str())
            .unwrap_or(DEFAULT_CONFIG_FILENAME)
            .to_string()
    });
    for (i, config) in configs.iter().enumerate() {
        // Create new filepath
        let filename = format!("{base_filename}{i:0>pad_size$}");
        let mut filepath = output_dir.clone();
        filepath.set_file_name(filename);
        if let Some(ext) = file.extension() {
            filepath.set_extension(ext);
        } else {
            filepath.set_extension(DEFAULT_TOML_EXTENSION);
        }

        let config_str = toml::to_string(config).context("Serializing unpacked configs as TOML")?;
        let mut new_file = std::fs::File::create(filepath).context("Creating file for TOML")?;
        write!(new_file, "{config_str}").context("Writing TOML to file")?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_toml() {
        let config_str = include_str!("./test.toml");
        let config: unpack::RangeConfig = toml::from_str(config_str).unwrap();
        println!("Config: {config:?}");
        println!("Config toml:\n{}", toml::to_string(&config).unwrap());
    }
}
