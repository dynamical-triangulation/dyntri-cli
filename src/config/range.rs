use serde::{Deserialize, Serialize};

pub type ValueOrList<T> = ValueOrRange<T, Vec<T>>;
pub type NumberOrRange<T> = ValueOrRange<T, number_range::NumberRange<T>>;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(untagged)]
pub enum ValueOrRange<T, R> {
    Value(T),
    Range(R),
}

impl<T, R> ValueOrRange<T, R>
where
    Vec<T>: From<R>,
{
    pub fn into_value_or_list(self) -> ValueOrList<T> {
        match self {
            ValueOrRange::Value(value) => ValueOrList::Value(value),
            ValueOrRange::Range(range) => ValueOrList::Range(Vec::from(range)),
        }
    }
}

impl<T> ValueOrList<T> {
    pub fn get_value(&self, index: usize) -> &T {
        match self {
            ValueOrRange::Value(value) => value,
            ValueOrRange::Range(range) => &range[index],
        }
    }
}

pub mod number_range {
    use serde::{Deserialize, Serialize};
    use std::ops::Add;

    pub use linear_range::LinRange;

    #[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
    #[serde(untagged)]
    pub enum NumberRange<T> {
        List(Vec<T>),
        Linear(LinRange<T>),
    }

    impl<T> From<NumberRange<T>> for Vec<T>
    where
        T: Add<Output = T> + PartialOrd + Copy,
    {
        fn from(range: NumberRange<T>) -> Self {
            match range {
                NumberRange::List(list) => list,
                NumberRange::Linear(linrange) => linrange.into(),
            }
        }
    }

    mod linear_range {
        use std::{mem, ops::Add};

        use super::*;

        /// Inclusive linear range allowing custom stepping
        #[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
        pub struct LinRange<T> {
            pub start: T,
            pub end: T,
            pub step: T,
        }

        struct Iter<T> {
            current: T,
            end: T,
            step: T,
        }

        impl<T: Copy> LinRange<T> {
            fn iter(&self) -> Iter<T> {
                Iter {
                    current: self.start,
                    end: self.end,
                    step: self.step,
                }
            }
        }

        impl<T> Iterator for Iter<T>
        where
            T: Add<Output = T> + PartialOrd + Copy,
        {
            type Item = T;

            fn next(&mut self) -> Option<Self::Item> {
                if self.current > self.end {
                    return None;
                }
                let new = self.current + self.step;
                Some(mem::replace(&mut self.current, new))
            }
        }

        impl<T> From<LinRange<T>> for Vec<T>
        where
            T: Add<Output = T> + PartialOrd + Copy,
        {
            fn from(range: LinRange<T>) -> Self {
                range.iter().collect()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use number_range::{LinRange, NumberRange};

    #[derive(Debug, Clone, Serialize, Deserialize)]
    struct TomlTest {
        test: NumberOrRange<i32>,
    }

    #[test]
    fn test_number_range_int() {
        const NUMBER_EXAMPLE_STR: &str = "test = 8";
        const LIST_EXAMPLE_STR: &str = "test = [1, 2, 5]";
        const LINRANGE_EXAMPLE_STR: &str = "test = {start = 3, end = 5, step = 2}";
        let list_example = ValueOrRange::Range(NumberRange::List(vec![1, 2, 5]));
        let number_example = ValueOrRange::Value(8);
        let linrange_example = ValueOrRange::Range(NumberRange::Linear(LinRange {
            start: 3,
            end: 5,
            step: 2,
        }));

        let vrange: TomlTest = toml::from_str(NUMBER_EXAMPLE_STR).unwrap();
        assert_eq!(vrange.test, number_example);
        let vrange: TomlTest = toml::from_str(LIST_EXAMPLE_STR).unwrap();
        assert_eq!(vrange.test, list_example);
        let vrange: TomlTest = toml::from_str(LINRANGE_EXAMPLE_STR).unwrap();
        assert_eq!(vrange.test, linrange_example);
    }
}
