use std::{path::PathBuf, str::FromStr};

use crate::{config::range::ValueOrRange, simulation::create_filename};

use super::{
    range::{NumberOrRange, ValueOrList},
    Config,
};
use anyhow::{Ok, Result};
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct RangeConfig {
    size: NumberOrRange<usize>,
    timeslices: NumberOrRange<usize>,
    measurements: NumberOrRange<usize>,
    burnin: NumberOrRange<usize>,
    interval: NumberOrRange<f32>,
    output_dir: Option<ValueOrList<PathBuf>>,
    base_filename: Option<String>,
}

impl RangeConfig {
    pub fn unzip(self) -> Result<Vec<Config>> {
        let size = self.size.into_value_or_list();
        let timeslices = self.timeslices.into_value_or_list();
        let measurements = self.measurements.into_value_or_list();
        let burnin = self.burnin.into_value_or_list();
        let interval = self.interval.into_value_or_list();
        let output_dir = self.output_dir;
        let base_filename = self.base_filename;

        // Check if sizes of each range are the same
        let mut amount = None;
        assert_vlist_size(&size, &mut amount)?;
        assert_vlist_size(&timeslices, &mut amount)?;
        assert_vlist_size(&measurements, &mut amount)?;
        assert_vlist_size(&burnin, &mut amount)?;
        assert_vlist_size(&interval, &mut amount)?;
        if let Some(dir) = &output_dir {
            assert_vlist_size(dir, &mut amount)?;
        }

        // If all single values just construct the single config
        let Some(amount) = amount else {
            let mut output_filepath = output_dir.map(unpack_value).unwrap_or_else(|| {
                PathBuf::from_str("./").expect("Can not create path at './'")  
            });
            let size =  unpack_value(size);
            output_filepath.set_file_name(base_filename.unwrap_or_else(|| create_filename(size)));
            return Ok(vec![Config {
                size,
                timeslices: unpack_value(timeslices),
                measurements: unpack_value(measurements),
                burnin: unpack_value(burnin),
                interval: unpack_value(interval),
                output_filepath: Some(output_filepath),
            }])
        };

        let order = (amount.ilog10() + 1) as usize;
        let configs = (0..amount)
            .map(|i| {
                let size = *size.get_value(i);
                let mut output_filepath = output_dir
                    .as_ref()
                    .map(|dir| dir.get_value(i).clone())
                    .unwrap_or_else(|| {
                        PathBuf::from_str("./").expect("Can not create path at './'")
                    });
                if let Some(base) = &base_filename {
                    output_filepath.set_file_name(format!("{base}-{i:0>order$}"));
                } else {
                    output_filepath.set_file_name(format!("{}{i:0>order$}", create_filename(size)));
                }
                Config {
                    size,
                    timeslices: *timeslices.get_value(i),
                    measurements: *measurements.get_value(i),
                    burnin: *burnin.get_value(i),
                    interval: *interval.get_value(i),
                    output_filepath: Some(output_filepath),
                }
            })
            .collect();
        Ok(configs)
    }
}

fn unpack_value<T>(vrange: ValueOrList<T>) -> T {
    use ValueOrRange::Value;
    let Value(value) = vrange else {
        panic!("Should be single value by previous checks")
    };
    value
}

#[derive(Error, Debug, Clone, Copy)]
#[error("Not all ranges have the same size")]
struct RangeSizeError;

fn assert_vlist_size<T>(vrange: &ValueOrList<T>, size: &mut Option<usize>) -> Result<()> {
    use ValueOrRange::Range;
    if let Range(list) = vrange {
        if *size.get_or_insert(list.len()) != list.len() {
            Err(RangeSizeError)?
        }
    }
    Ok(())
}
