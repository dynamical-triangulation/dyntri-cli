use clap::Parser;

mod cli;
mod config;
mod geometry;
mod simulation;
mod visualisation;

fn main() {
    let cli = cli::Cli::parse();

    let mut log_builder = env_logger::Builder::from_default_env();
    log_builder.filter_level(cli.logging).init();

    use cli::Task::{Config, Geometry, Simulation, Visualisation};
    match cli.task {
        Config {
            config_task: command,
        } => config::perform_config_task(command),
        Simulation {
            config_file,
            temp_dir,
            observables,
        } => simulation::run_cdt_simulation(config_file, temp_dir, observables),
        Geometry {
            configuration,
            output_file: config_file,
            dir_in,
            temp_dir,
        } => geometry::perform_geometry_measurement(
            config_file.as_path(),
            dir_in.as_path(),
            configuration,
            temp_dir.as_path(),
        ),
        Visualisation {
            visualisation_type,
            output_filename,
        } => visualisation::create_visualisation(visualisation_type, output_filename.as_path()),
    }
}
