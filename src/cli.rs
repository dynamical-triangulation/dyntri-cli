use std::path::PathBuf;

use clap::{command, Parser, Subcommand};

use crate::{geometry::Configuration, simulation::ObservableSet, visualisation::Embedding};

/// bla bal
#[derive(Parser)]
#[command(author, version, about, long_about)]
pub struct Cli {
    /// Sets the logging level for the application.
    ///
    /// Possible values are: [error, warn, info, debug, trace]
    #[arg(short = 'l', long, default_value = "info", value_name = "LEVEL")]
    pub logging: log::LevelFilter,
    #[command(subcommand)]
    pub task: Task,
}

#[derive(Subcommand)]
pub enum Task {
    /// Runs the CDT Monte Carlo simulation
    Simulation {
        config_file: PathBuf,
        /// Directory to store temporary files in.
        ///
        /// Note that for some observables, namely global distance measurements a
        /// distance matrix may be stored in this directory and can get very large,
        /// so make sure there is enough disk space.
        #[arg(short, long, value_name = "DIR_PATH", default_value = "./")]
        temp_dir: PathBuf,
        #[arg(short, long, value_enum, default_value = "full-graph")]
        observables: ObservableSet,
    },
    /// Performs measurements on fixed geometry
    Geometry {
        output_file: PathBuf,
        /// Also functioning as single file in for Single and DiffusionPeriodic
        #[arg(short, long, value_name = "INPUT_DIR_PATH", default_value = "./")]
        dir_in: PathBuf,
        /// Also functioning as boundary file in DiffusionPeriodic
        #[arg(short, long, value_name = "DIR_PATH", default_value = "./")]
        temp_dir: PathBuf,
        #[arg(short, long, value_enum, default_value = "single")]
        configuration: Configuration,
    },
    /// Config parsing and generation utilities
    Config {
        #[command(subcommand)]
        config_task: ConfigTask,
    },
    /// Triangulation visualisation
    Visualisation {
        /// The type of visualisation to use
        #[arg(short = 't', long = "type", value_enum)]
        visualisation_type: Embedding,
        #[arg(short, long, value_name = "OUTPUT_PATH")]
        output_filename: PathBuf,
    },
}

#[derive(Subcommand)]
pub enum ConfigTask {
    New,
    Unpack {
        /// The file containing the extended configuration that need to be unpacked
        file: PathBuf,
        /// The directory to which the unpacked config files are output
        #[arg(short = 'd', long, value_name = "DIR_PATH", default_value = "./")]
        output_dir: PathBuf,
        /// The base filename for the output config files
        ///
        /// This name is used as a base for the output files, each will additionally
        /// get a number added to it's name when unpacking
        #[arg(short, long, value_name = "FILENAME")]
        output_filename: Option<String>,
    },
}
